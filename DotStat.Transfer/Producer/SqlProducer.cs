﻿using DotStat.Common.Auth;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db;
using DotStat.Db.Exception;
using DotStat.Db.Service;
using DotStat.Db.Util;
using DotStat.DB.Repository;
using DotStat.Domain;
using DotStat.MappingStore;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using Estat.Sdmxsource.Extension.Constant;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Constants.InterfaceConstant;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Data.Query;
using Org.Sdmxsource.Sdmx.StructureRetrieval.Manager;
using Org.Sdmxsource.Sdmx.Util.Objects.Container;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace DotStat.Transfer.Producer
{
    ///TODO: Once used, include in unit tests
    [ExcludeFromCodeCoverage]
    public class SqlProducer<T> : IProducer<T> where T: TransferParam, IFromSqlTransferParam
    {
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IMappingStoreDataAccess _mappingStoreDataAccess;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        public SqlProducer(
            IAuthorizationManagement authorizationManagement,
            IMappingStoreDataAccess mappingStoreDataAccess,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver
        )
        {
            _authorizationManagement = authorizationManagement;
            _mappingStoreDataAccess = mappingStoreDataAccess;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        public Task<IImportReferenceableStructure> GetReferencedStructure(T transferParam, bool throwErrorIfNotFound = true)
        {
            var referencedStructure = transferParam.SourceReferencedStructure.Type == ReferencedStructureType.Dsd ?
                _mappingStoreDataAccess.GetDsd(
                    transferParam.SourceDataspace.Id,
                    transferParam.SourceReferencedStructure.AgencyId,
                    transferParam.SourceReferencedStructure.Id,
                    transferParam.SourceReferencedStructure.Version,
                    throwErrorIfNotFound
                ) : (IImportReferenceableStructure)_mappingStoreDataAccess.GetDataflow(
                    transferParam.SourceDataspace.Id,
                    transferParam.SourceReferencedStructure.AgencyId,
                    transferParam.SourceReferencedStructure.Id,
                    transferParam.SourceReferencedStructure.Version,
                    throwErrorIfNotFound
               );

            if (transferParam.DestinationDataspace != null)
            {
                Log.Notice(string.Format(
                        LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.DataflowLoaded,
                            transferParam.CultureInfo.TwoLetterISOLanguageName),
                        referencedStructure.FullId,
                        transferParam.SourceDataspace.Id));
            }

            return Task.FromResult(referencedStructure);
        }

        public async Task<TransferContent> Process(T transferParam, IImportReferenceableStructure referencedStructure, CancellationToken cancellationToken)
        {
            if (!IsAuthorized(transferParam, referencedStructure))
            {
                throw new TransferUnauthorizedException();
            }

            var dotStatDbService = _dotStatDbServiceResolver(transferParam.SourceDataspace?.Id);
            var unitOfWork = _unitOfWorkResolver(transferParam.SourceDataspace?.Id);
            
            await dotStatDbService.FillIdsFromDisseminationDb(referencedStructure, cancellationToken);

            var codeTranslator = new CodeTranslator(unitOfWork.CodeListRepository);
            await codeTranslator.FillDict(referencedStructure, cancellationToken);

            var content = new TransferContent();
            var sourceDbTableVersion = transferParam.SourceVersion == TargetVersion.Live
                ? dotStatDbService.GetDsdLiveVersion(referencedStructure.Dsd)
                : dotStatDbService.GetDsdPITVersion(referencedStructure.Dsd);
            
            if (sourceDbTableVersion == null)
                throw new TableVersionException($"The DSD {referencedStructure.Dsd.FullId} does not contain a {transferParam.SourceVersion} version.");
            var hasMetadata = referencedStructure.Dsd?.Msd != null;

            DataQueryImpl dataQuery = null;

            if (referencedStructure is Dataflow dataFlow)
            {
                var sdmxObjects = new SdmxObjectsImpl(dataFlow.Dsd.Base, dataFlow.Base);
                
                if (hasMetadata)
                {
                    sdmxObjects.AddMetadataStructure(dataFlow.Dsd.Msd.Base);
                }

                dataQuery = new DataQueryImpl(transferParam.SourceQuery, new InMemoryRetrievalManager(sdmxObjects))
                {
                    IncludeHistory = true
                };
            }

            switch (transferParam.TransferType)
            {
                case TransferType.DataAndMetadata:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        DatasetAttributes = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                        //Attributes not attached to the time dimension
                        SeriesAttributesWithNoTimeDim = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                            .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                            .ToList(),

                        //Attributes attached to the time dimension
                        ObservationAttributes = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                            || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                        Dimensions = referencedStructure.Dsd.Dimensions.ToList(),
                        TimeDimension = referencedStructure.Dsd.TimeDimension,
                        IsPrimaryMeasureReported = true,
                        MetadataAttributes =
                            hasMetadata ? referencedStructure.Dsd.Msd.MetadataAttributes : new List<MetadataAttribute>()
                    };


                    content.DataObservations = unitOfWork.ObservationRepository.GetObservations(
                        dataQuery,
                        referencedStructure,
                        (DbTableVersion)sourceDbTableVersion,
                        isDataQuery: true,
                        cancellationToken
                    );

                    if (hasMetadata) {
                        content.MetadataObservations = unitOfWork.ObservationRepository.GetObservations(
                            dataQuery,
                            referencedStructure,
                            (DbTableVersion)sourceDbTableVersion,
                            isDataQuery: false,
                            cancellationToken
                        ); 
                    }

                    //DataSet Level attributes already extracted in observations
                    content.DatasetAttributes = new List<DataSetAttributeRow>();

                    break;
                }
                case TransferType.DataOnly:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        DatasetAttributes = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.DataSet || a.Base.AttachmentLevel == AttributeAttachmentLevel.Null).ToList(),

                        //Attributes not attached to the time dimension
                        SeriesAttributesWithNoTimeDim = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Group || a.Base.AttachmentLevel == AttributeAttachmentLevel.DimensionGroup)
                            .Where(a => !a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId))
                            .ToList(),

                        //Attributes attached to the time dimension
                        ObservationAttributes = referencedStructure.Dsd.Attributes
                            .Where(a => a.Base.AttachmentLevel == AttributeAttachmentLevel.Observation
                            || a.Base.GetDimensionReferences().Contains(DimensionObject.TimeDimensionFixedId)).ToList(),
                        Dimensions = referencedStructure.Dsd.Dimensions.ToList(),
                        TimeDimension = referencedStructure.Dsd.TimeDimension,
                        IsPrimaryMeasureReported = true,
                    };

                    content.DataObservations = unitOfWork.ObservationRepository.GetObservations(
                        dataQuery,
                        referencedStructure,
                        (DbTableVersion)sourceDbTableVersion,
                        isDataQuery: true,
                        cancellationToken
                    );

                    //DataSet Level attributes already extracted in observations
                    content.DatasetAttributes = new List<DataSetAttributeRow>();
                    
                    break;
                }
                case TransferType.MetadataOnly:
                {
                    content.ReportedComponents = new ReportedComponents
                    {
                        Dimensions = referencedStructure.Dsd.Dimensions.ToList(),
                        TimeDimension = referencedStructure.Dsd.TimeDimension,
                        MetadataAttributes =
                            hasMetadata ? referencedStructure.Dsd.Msd.MetadataAttributes : new List<MetadataAttribute>()
                    };

                    if (hasMetadata)
                        content.MetadataObservations = unitOfWork.ObservationRepository.GetObservations(
                            dataQuery,
                            referencedStructure,
                            (DbTableVersion)sourceDbTableVersion,
                            isDataQuery: false,
                            cancellationToken
                        );

                    break;
                }
            }

            return content;
        }

        public bool IsAuthorized(T transferParam, IImportReferenceableStructure referencedStructure)
        {
            return _authorizationManagement.IsAuthorized(
                transferParam.Principal, 
                transferParam.SourceDataspace.Id,
                referencedStructure.AgencyId,
                referencedStructure.Code,
                referencedStructure.Version.ToString(), 
                PermissionType.CanReadData
            );
        }

        public void Dispose()
        {
            //Nothing to do, sql connection  is within a "using" clause
        }
    }
}