using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Db.Dto;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using log4net.Core;
using Microsoft.AspNetCore.WebUtilities;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Api.Engine;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.Api.Model.Query;
using Org.Sdmxsource.Sdmx.DataParser.Engine.Csv;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.DataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Mutable.MetadataStructure;
using Org.Sdmxsource.Sdmx.SdmxObjects.Model.Objects.Reference;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Org.Sdmxsource.Sdmx.Api.Model.Mutable.Base;
using MediaTypeHeaderValue = Microsoft.Net.Http.Headers.MediaTypeHeaderValue;
using Org.Sdmxsource.Sdmx.Api.Model.Objects.Base;
using Org.Sdmxsource.Sdmx.Api.Model.Data.Query;

namespace DotStat.Transfer.Utils
{
    public static class Extensions
    {
        public static readonly HashSet<string> AllowedImportSchemes = new HashSet<string>()
        {
            Uri.UriSchemeHttp,
            Uri.UriSchemeHttps
        };


        public static DataspaceInternal GetSpaceInternal(this string space, IDataspaceConfiguration configuration, string lang, bool mandatory = true)
        {
            if (string.IsNullOrEmpty(space) && mandatory)
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotProvided, lang));

            var dataspace = configuration
                                .SpacesInternal
                                .FirstOrDefault(x => x.Id.Equals(space, StringComparison.InvariantCultureIgnoreCase));

            if (dataspace == null && mandatory)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataSpaceNotFound, lang),
                    space));

            return dataspace;
        }

        public static void SupportsTargetVersion(this DataspaceInternal dataSpace, TargetVersion targetVersion,
            string lang)
        {
            if (dataSpace.KeepHistory && targetVersion == TargetVersion.PointInTime)
            {
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                        .PITAndKeepHistoryNotSupportedError, lang),
                    dataSpace.Id)
                );
            }
        }

        public static bool TempLocalFileExists(this string fileLocalPath, string fileName, string lang)
        {
            if (string.IsNullOrEmpty(fileLocalPath) || !File.Exists(fileLocalPath))
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.FileAttachmentNotProvided,
                        lang), fileName));
            return true;
        }

        public static bool IsValidExtension(this string fileName, string fieldName, string lang, params string[] extensions)
        {
            if (string.IsNullOrEmpty(fileName) || !extensions.Any(ext => fileName.EndsWith(ext, StringComparison.OrdinalIgnoreCase)))
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidFileExtension, lang),
                    fieldName, fileName, string.Join(",", extensions)));
            return true;
        }
        public static bool IsValidContentType(this string contentType, string fieldName, string lang, params string[] validTypes)
        {
            if (validTypes == null)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ContentTypesUndefined, lang),
                    fieldName));

            if (string.IsNullOrEmpty(contentType) || !validTypes.Any(type => contentType.Equals(type, StringComparison.InvariantCultureIgnoreCase)))
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NotValidContentType, lang),
                    fieldName, string.Join(",", validTypes), fieldName));

            return true;
        }

        public static Encoding GetEncoding(this MultipartSection section)
        {
            var hasMediaTypeHeader = MediaTypeHeaderValue.TryParse(section.ContentType, out var mediaType);
            // UTF-7 is insecure and should not be honored. UTF-8 will succeed in 
            // most cases.
            if (!hasMediaTypeHeader || Encoding.UTF7.Equals(mediaType.Encoding))
            {
                return Encoding.UTF8;
            }
            return mediaType.Encoding;
        }

        public static DataflowMutableCore GetDataflow(this string dataflow, string lang)
        {
            if (string.IsNullOrEmpty(dataflow))
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = dataflow.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    dataflow));

            return new DataflowMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static IdentifiableReferencedStructure GetReferencedStructure(this string referencedStructure, string lang, ReferencedStructureType referencedStructureType)
        {
            if (string.IsNullOrEmpty(referencedStructure))
            {
                var localizedResource = referencedStructureType == ReferencedStructureType.Dsd ? 
                    Localization.ResourceId.DSDNotProvided : Localization.ResourceId.DataFlowNotProvided;
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(localizedResource, lang));
            }

            var elements = referencedStructure.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
            {
                var localizedResource = referencedStructureType == ReferencedStructureType.Dsd ?
                    Localization.ResourceId.WrongDSDFormat : Localization.ResourceId.WrongDataFlowFormat;

                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(localizedResource, lang),
                    referencedStructure));
            }

            return new IdentifiableReferencedStructure
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2],
                Type = referencedStructureType
            };
        }

        public static string FullId(this IDataflowMutableObject df)
        {
            if (df == null)
                throw new ArgumentException(nameof(df));

            return $"{df.AgencyId}:{df.Id}({df.Version})";
        }

        public static string FullId(this IMaintainableObject df)
        {
            if (df == null)
                throw new ArgumentException(nameof(df));

            return $"{df.AgencyId}:{df.Id}({df.Version})";
        }

        public static ArtefactItem GetArtefact(this string artefact, string lang)
        {
            if (string.IsNullOrEmpty(artefact))
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DataFlowNotProvided, lang));

            var elements = artefact.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDataFlowFormat, lang),
                    artefact));

            return new ArtefactItem()
            {
                Agency = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static DataStructureMutableCore GetDsd(this string dsd, string lang)
        {
            if (string.IsNullOrEmpty(dsd))
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DSDNotProvided, lang));

            var elements = dsd.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDSDFormat, lang),
                    dsd));

            return new DataStructureMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        public static MetadataStructureDefinitionMutableCore GetMsd(this string msd, string lang)
        {
            if (string.IsNullOrEmpty(msd))
                throw new DotStatException(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.MSDNotProvided, lang));

            var elements = msd.Split(new[] { ':', '(', ')' }, StringSplitOptions.RemoveEmptyEntries);

            if (elements.Length != 3)
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongMSDFormat, lang),
                    msd));

            return new MetadataStructureDefinitionMutableCore()
            {
                AgencyId = elements[0],
                Id = elements[1],
                Version = elements[2]
            };
        }

        // ReSharper disable once InconsistentNaming
        public static DateTime? GetPITReleaseDate(this string dateTime, string lang)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;

            // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
            const string validationExpression =
                @"^([1-9][0-9]{3,})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])T(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])(\.[0-9]{1,3})?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

            if (Regex.IsMatch(dateTime, validationExpression) && DateTime.TryParse(dateTime, out var dateTimeValue))
            {
                return dateTimeValue; // Return the local (server) date time value
            }

            throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDateFormat, lang),
                    "PITReleaseDate", dateTime, "YYYY-MM-DDThh:mm:ss.sTZD"));
        }

        public static bool IsValidUrl(string url)
        {
            return Uri.TryCreate(url, UriKind.Absolute, out var uriResult) && AllowedImportSchemes.Contains(uriResult.Scheme);
        }

        public static string GetTransactionOutcome(TransactionStatus status, TransactionLog[] transactionLogs)
        {
            if (status == TransactionStatus.Queued || status == TransactionStatus.InProgress || status == TransactionStatus.Unknown
                || !transactionLogs.Any()) //not yet finish processing or no logs
            {
                return TransactionOutcome.None.ToString();
            }

            if (status == TransactionStatus.TimedOut || status == TransactionStatus.Canceled)
            {
                return TransactionOutcome.Error.ToString();
            }

            if (transactionLogs.Any(l => l.Level.Equals(Level.Fatal.Name, StringComparison.OrdinalIgnoreCase) ||
                                         l.Level.Equals(Level.Error.Name, StringComparison.OrdinalIgnoreCase))) //at least one error or fatal
            {
                return TransactionOutcome.Error.ToString();
            }

            if (transactionLogs.Any(l => l.Level.Equals(Level.Warn.Name, StringComparison.OrdinalIgnoreCase))) //at least one warning
            {
                return TransactionOutcome.Warning.ToString();
            }

            return TransactionOutcome.Success.ToString();
        }

        public static bool IsValidEmail(this string email, bool throwError = false)
        {
            try
            {
                return new System.Net.Mail.MailAddress(email).Address == email;
            }
            catch
            {
                return throwError
                    ? throw new DotStatException(
                        message: LocalizationRepository.GetLocalisedResource(
                            Localization.ResourceId.EmailParameterError))
                    : false;
            }
        }
        
        public static DateTime? GetDateTimeInUtc(this string dateTime, string parameterName, string lang)
        {
            if (string.IsNullOrEmpty(dateTime))
                return null;

            //Keep backward compatible format from DLM logbook
            const string backwardCompatibleFormat = "dd-MM-yyyy HH:mm:ss";
            if (DateTime.TryParseExact(dateTime, backwardCompatibleFormat, CultureInfo.InvariantCulture,
                    DateTimeStyles.None, out var dateTimeValue))
                return dateTimeValue.ToUniversalTime();

            // DateTime does not support 24th hour in accordance with ISO ISO 8601-1:2019: it is explicitly disallowed by the 2019 revision.
            const string validationExpression =
                @"^([1-9][0-9]{3,})-(0[1-9]|1[0-2])-(0[1-9]|[12][0-9]|3[01])(T|\s{1})(([01][0-9]|2[0-3]):[0-5][0-9]:[0-5][0-9])(\.[0-9]{1,3})?(Z|(\+|-)((0[0-9]|1[0-3]):[0-5][0-9]|14:00))?$";

            if (!Regex.IsMatch(dateTime, validationExpression) || !DateTime.TryParse(dateTime, out dateTimeValue))
                throw new DotStatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.WrongDateFormat, lang),
                    parameterName, dateTime, "dd-MM-yyyy HH:mm:ss or YYYY-MM-DDThh:mm:ssTZD"));

            return dateTimeValue.ToUniversalTime();
        }

        public static IRestDataQuery GetRestDataQuery(this string sourceQuery, IdentifiableReferencedStructure sourceReferencedObject, string lang)
        {
            var filter =  string.IsNullOrEmpty(sourceQuery) ? "all" : sourceQuery;
            var dataQuery = new RESTDataQueryCore(string.Format("data/{0},{1},{2}/{3}",
                sourceReferencedObject.AgencyId,
                sourceReferencedObject.Id,
                sourceReferencedObject.Version,
                filter));

            if(dataQuery.UpdatedAfter is null)
            {
                return dataQuery;
            }

            var offset = dataQuery.UpdatedAfter.DateWithOffset.Offset;
            if (offset == TimeSpan.Zero)
            {
                Log.Warn(string.Format(LocalizationRepository.GetLocalisedResource(
                    Localization.ResourceId.NoTimeZoneProvidedForParameter, lang), "updatedAfter in the sourceQuery", offset));

                var updateAfterStr = TimeZoneInfo.ConvertTime(dataQuery.UpdatedAfter.Date, TimeZoneInfo.Local).ToString("yyyy'-'MM'-'dd'T'HH':'mm':'ss'.'fff'Z'");
                dataQuery.SetUpdatedAfter(updateAfterStr);
            }

            return dataQuery;
        }

        public static CancellationTokenSource SetBackgroundTaskCompletionSteps(this CancellationToken backgroundCancellationToken, IDotStatDbService dotStatDbService, TransferParam transferParam, CancellationToken cancellationToken)
        {
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            var internalCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(backgroundCancellationToken, cancellationToken);
            if (transferParam.DestinationDataspace.DataImportTimeOutInMinutes > 0)
            {
                //Cancel Token after the time set in the configuration for this dataspace
                internalCancellationToken.CancelAfter(TimeSpan.FromMinutes(transferParam.DestinationDataspace.DataImportTimeOutInMinutes));
            }

            //Mark transaction as timedOut or aborted when the internalCancellationToken is canceled
            internalCancellationToken.Token.Register(async () => await dotStatDbService.CloseCanceledOrTimedOutTransaction(transferParam.Id));

            return internalCancellationToken;
        }

        public static async Task SetStillProcessingLogging(this CancellationToken cancellationToken, TransferParam transferParam)
        {
            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
            
            using var internalCancellationToken = CancellationTokenSource.CreateLinkedTokenSource(cancellationToken);
            if (transferParam.DestinationDataspace.DataImportTimeOutInMinutes > 0)
            {
                //Cancel Token after the time set in the configuration for this dataspace
                internalCancellationToken.CancelAfter(TimeSpan.FromMinutes(transferParam.DestinationDataspace.DataImportTimeOutInMinutes));
            }

            //Log "Still Processing..." until the job is completed or canceled
            while (!internalCancellationToken.Token.IsCancellationRequested)
            {
                await Task.Delay(TimeSpan.FromMinutes(transferParam.DestinationDataspace.NotifyStillInProcessEveryMinutes), cancellationToken);
                Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StillProcessing, transferParam.CultureInfo.TwoLetterISOLanguageName));
            }
        }

        public static StagingRowActionEnum GetStagingRowActionEnum(this IDataReaderEngine dataReaderEngine)
        {
            return dataReaderEngine.CurrentAction.ToDatasetActionEnumType() switch
            {
                DatasetActionEnumType.Append => StagingRowActionEnum.Merge,
                DatasetActionEnumType.Null => StagingRowActionEnum.Merge,
                DatasetActionEnumType.Information => StagingRowActionEnum.Merge,
                DatasetActionEnumType.Merge => StagingRowActionEnum.Merge,
                DatasetActionEnumType.Delete => StagingRowActionEnum.Delete,
                DatasetActionEnumType.Replace => StagingRowActionEnum.Replace,
                _ => throw new NotSupportedException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ActionNotSupported))
            };
        }

        public static bool IsXml(this IDataReaderEngine reader)
        {
            return reader is not CsvDataReaderEngine && reader is not CsvDataReaderEngineV2;
        }
        
        public static void CheckAuthorized(this StagingRowActionEnum action, bool canMergeData, bool canDeleteData, bool canReplaceData, int rowDsdNumber, bool isXml)
        {            
            switch (action)
            {
                case StagingRowActionEnum.Merge:
                    if (!canMergeData)
                    {
                        var unauthorizedToMergeText = isXml
                            ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeXml), rowDsdNumber)
                            : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeCsv), rowDsdNumber, rowDsdNumber + 1);

                        throw new TransferUnauthorizedException(unauthorizedToMergeText);
                    }
                    break;
                case StagingRowActionEnum.Replace:
                    if (!canReplaceData)
                    {
                        var unauthorizedToMergeText = isXml
                            ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeXml), rowDsdNumber)
                            : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToMergeCsv), rowDsdNumber, rowDsdNumber + 1);

                        throw new TransferUnauthorizedException(unauthorizedToMergeText);
                    }
                    break;
                case StagingRowActionEnum.Delete:
                    if (!canDeleteData)
                    {
                        var unauthorizedToDeleteText = isXml
                                ? string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteXml), rowDsdNumber)
                                : string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.UnauthorizedToDeleteCsv), rowDsdNumber, rowDsdNumber + 1);
                        throw new TransferUnauthorizedException(unauthorizedToDeleteText);
                    }
                    break;
                default:
                    throw new NotSupportedException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.ActionNotSupported));
            }
        }

        public static DataCompressionEnum GetDataCompression(this DataspaceInternal dataspace)
        {
            return dataspace.Archive ? DataCompressionEnum.COLUMNSTORE_ARCHIVE : DataCompressionEnum.NONE;
        }

        public static bool SendEmail(this SendEmailOptionsEnum sendEmailOption, TransactionStatus transactionStatus, bool hasErrors)
        {
            switch (sendEmailOption)
            {
                case SendEmailOptionsEnum.Never:
                    return false;
                case SendEmailOptionsEnum.Always:
                    return true;
                case SendEmailOptionsEnum.IfError when hasErrors:
                    return true;
                case SendEmailOptionsEnum.IfError when transactionStatus == TransactionStatus.TimedOut:
                    return true;
                case SendEmailOptionsEnum.IfError when transactionStatus == TransactionStatus.Canceled:
                    return true;
                default:
                    return false;
            }
        }
    }
}
