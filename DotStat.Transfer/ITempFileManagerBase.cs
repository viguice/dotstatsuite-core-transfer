﻿namespace DotStat.Transfer
{
    public interface ITempFileManagerBase
    {
        public string GetTempFileName(string prefix = null);
    }
}
