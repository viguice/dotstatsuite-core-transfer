﻿using DotStat.Common.Configuration;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Param;
using DotStat.Transfer.Producer;
using System;
using System.Threading;
using System.Threading.Tasks;
using DotStat.Domain;

namespace DotStat.Transfer.Manager
{
    public class UrlTransferManager<T> : TransferManager<T> where T : ITransferParamWithUrl
    {
        public UrlTransferManager(BaseConfiguration configuration, IProducer<T> observationProducer, IConsumer<T> observationConsumer) : base(configuration, observationProducer, observationConsumer)
        {
        }

        public override async Task Transfer(T transferParam, Transaction transaction, CancellationToken cancellationToken)
        {
            if (transferParam == null)
            {
                throw new ArgumentNullException(nameof(transferParam));
            }

            var referencedStructure = await Producer.GetReferencedStructure(transferParam);

            if (!Consumer.IsAuthorized(transferParam, referencedStructure))
            {
                throw new TransferUnauthorizedException();
            }

            var transferContent = await Producer.Process(transferParam, referencedStructure, cancellationToken);

            var dsd = referencedStructure.GetType() == typeof(Dsd) ?
                referencedStructure as Dsd : (referencedStructure as Dataflow).Dsd;
            if (!HasComponentsToProcess(dsd, transferContent))
            {
                return;
            }

            if (!await Consumer.Save(transferParam, transaction, referencedStructure, transferContent, cancellationToken))
            {
                throw new TransferFailedException();
            }

            Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.StreamingFinished, transferParam.CultureInfo.TwoLetterISOLanguageName));
        }
    }
}
