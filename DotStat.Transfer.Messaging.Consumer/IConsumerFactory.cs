﻿using DotStat.Db.Service;
using DotStat.Transfer.Interface;
using Microsoft.Extensions.Configuration;
using System;

namespace DotStat.Transfer.Messaging.Consumer
{
    public interface IConsumerFactory
    {
        IConsumer Create(IServiceProvider serviceProvider, IConfiguration configuration, IMailService mailService, DotStatDbServiceResolver dotStatDbServiceResolver);
        
    }


}
