# HISTORY

## v16.0.2

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.6
- CommonDB v3.8

### Issues
- [#696](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/696) Removed index reorganization from import logic. This should be part of index optimization sql job.


## v16.0.1

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.6
- CommonDB v3.8

### Issues
- [#393](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/393) Allow setting MinPercentageDiskSpace to 0 
- [#677](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/677) Corrected the disk space availability calculation on very large disks
- [#709](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/709) Fixed errors in dbup script occuring when line endings are replaced to CRLF + fixed typo in MSD deleted view creation


## v16.0.0

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.5
- CommonDB v3.8

### Issues
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/127) Fixed migration of meta tables with non-coded dimensions
- [#683](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/683) Fixed isssue of getting the same Delete records multiple times at imports and transfers
- [#692](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/692) Fixed issue with big obs count
- [#695](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/695) Systematically added UnhandledExceptionOccurred ERROR on all requests that end up in a FATAL error
- [#696](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/696) Added feature to reorganize all columnstore indexes at the end of imports
- [#698](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/698) Fixed data upload issues with non-coded dimensions
- [#699](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/699) Changed transfer message from 'Warning' to 'Error' in case of wrong data upload (minimum of one non-dimension component is required)
- [#700](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/700) Excluded non-coded dimensions during the calculation of the actual content constraint
- [#705](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/705) Added REST api v3, removed v2.1, deprecated v2


## v15.0.2

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.3
- CommonDB v3.8

### Issues
- [#58](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/58) Updated .gitlab-ci.yml file to run performance tests on a daily basis
- [#170](https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/170) Updated LICENSE file
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/403) Removed concurrency error for requeued requests
- [#528](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/528) Changed transaction and transaction logs datetime fields to store values in UTC
- [#638](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/638) Fixed bug at validation of dataset attributes at Delete action
- [#650](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/650) Fixed check of mandatory attributes at dataflow level
- [#653](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/653) Fixed release resource issue
- [#655](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/655) Fixed bug of mandatory OBS_STATUS values disappearing while transferring data from one space to another
- [#658](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/658) Applied bug fix to allow missing mandatory dataset attribute with Replace or Merge action
- [#659](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/659) Fixed bug when only dataflow level attributes are present in a replace action
- [#660](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/660) Fixed missing Measure column issue at Replace action
- [#664](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/664) Fixed 'The given ColumnMapping does not match up with any column in the source or destination' error when uploading data
- [#665](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/665) Fixed release resource issue
- [#669](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/669) Fixed duplicated coordinate error at transfer of a dataflow with no dataset attribute value provided 
- [#673](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/673) Extended delete functionality, delete components according to the storage level


## v15.0.1

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.0
- CommonDB v3.8

### Issues
- [#652](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/652) Fix migration script recreate MSD DF view when DSD does not have time dimension


## v15.0.0

### Database compatibility
- StructureDB v6.20
- DataDB v8.1.0
- CommonDB v3.8

### Issues
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/53) Aligned transfer service with dbup in postman tests and fixed postman job for schedule
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/126) Changed recreation of mapping sets and recalculation of actual content constraints to be done always for DSD import/transfer, improved mapping set creating call when dsd has multiple dataflows and fixed the bug causing mapping sets not created when import has a DSD reference
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/402) Updated nuget references to match with ESTAT v8.18.6
- [#407](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/407) Changed precision of SQL datatype of float & double components types and added new localization text for the case when bulk copy fails with value length exceeding the maximum allowed by SQL data type
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/410) Updated nuget references to match with ESTAT v8.18.7
- [#568](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/568) Added postman template
- [#596](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/596) Extended transfer service to allow importing and transfering referential metadata referencing a DSD and changed behaviour of transfer/dataflow to only transfer referential metadata attached to dataflow
- [#623](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/623) Fixed the bug identified at transfers with updatedAfter
- [#624](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/624) Fixed the bug causing the second PIT not released
- [#625](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/625) Fixed transfer with non-coded dimension
- [#628](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/628) Added functionality to cancel unfinished jobs when transfer service instance restarts
- [#630](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/630) Fixed bug of dsd's db id not being initialized at calculation of availability 
- [#631](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/631) Fixed init/allMappingsets wrongly failing on external dataflows
- [#634](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/634) Fixed failing cases of LastUpdatedUtcDataQueryTest
- [#646](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/646) Removed unnecessary check for not-logged-in users in transaction status methods


## v14.0.0

### Database compatibility
- StructureDB v6.20
- DataDB v7.7.0
- CommonDB v3.8

### Issues
- [#85](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/85) Update of docker image description to avoid duplication of configuration documentation
- [#338](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/338) Made cleanupDsd function transactional
- [#375](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/375) Advanced data validation should not block data uploads because of invalid DB content
- [#392](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/392) Updated nuget references to match with ESTAT v8.18.4
- [#395](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/395) Fixed error occured in transfers of referential metadata without data
- [#456](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/456) Changed localization messages for missing dimensions in CSV and non-defined attributes
- [#457](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/457) Improved performance of cleaning mappingsets
- [#576](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/576) Introduced Temporal Tables to support includeHistory and As Of features for Data
- [#598](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/598) Added case sensitive comparison for non-coded component values
- [#601](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/601) Granted CONTROL rights to DotStatWriter role on data schema
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/603) Improved performance of init/allmappingsets
- [#614](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/614) Mail sending is not attempted if smtp host not configured 
- [#615](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/615) Fixed collate error when uploading data with monthly time period date
- Limited no. of parallel execution threads for InitializeAllMappingSets task


## v13.0.1

### Database compatibility
- StructureDB v6.20
- DataDB v7.5.0
- CommonDB v3.8

### Issues
- [#587](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/587) Fixed bug causing actual content constraints being not re-calculated when advanced validation is applied


## v13.0.0

### Database compatibility
- StructureDB v6.20
- DataDB v7.5.0
- CommonDB v3.8

### Issues
- [#46](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/46) Improved texts of AttributeDoesNotBelongToDataset and DimensionDoesNotBelongToDataset localizations
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/126) Implemented non-coded dimension support
- [#158](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/158) Added advanced validations for mandatory attributes at observation level
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/306) Implemented support of confidential observations
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/313) Added new optional 'sendEmail' parameter to transfer servce methods indicating when emails should be sent
- [#374](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/374) Updated nuget references to match with ESTAT v8.18.0
- [#377](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/377) Updated nuget references to match with ESTAT v8.18.2
- [#534](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/534) Added capability to compress DSD  
- [#545](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/545) Improved management of deleted values
- [#574](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/574) Fix dataflow level attribute import without dimensions
- [#578](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/578) Fixed error at data transfer with advanced validations falsely reporting duplicated observations
- [#579](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/579) Applied usage of normal views for transfering data when updatedAfter parameter is not provided 
- [#580](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/580) Improved dsd reference error message
- [#581](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/581) Fixed broken bulk deletion
- [#582](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/582) Fixed broken DF attr deletion
- [#583](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/583) Fixed error at transfer of ref. metadata (insert + merge + delete)
- [#584](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/584) Fixed the issue with metadata merges mistakenly doing replaces in certain cases
- [#585](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/585) Fix vulnerabilities of siscc/dotstatsuite-core-transfer image revealed by docker scout
- [#588](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/588) Fixed the issue at deletion of all attributes of a dataflow using SDMX-ML data file


## v12.0.0

### Database compatibility
- StructureDB v6.20
- DataDB v7.4.0
- CommonDB v3.8

### Issues
- [#30](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/30) Added transfer of deleted data between dataspace
- [#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/57) Added updatedAfter feature (timestamp updated/inserted or deleted)
- [#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-docker-compose/-/issues/70) Moved CORS setup before healt check setup
- [#93](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/93) Changed SQL inline string values to sql execution parameters for special values
- [#95](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/95) Fixed treatment of invalid codes of allowed CC at actual CC creation
- [#99](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/99) Added last_updated info to all storage levels of components for updatedAfter feature
- [#100](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/100) Fixed the View typo that prevented migration when there was Dataset lvl attribute with codelist
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/105) Improved performance of observation validation function
- [#106](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/106) Added memory cache for validated time values
- [#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/124) Removed un-used dependency which contains CRITICAL security issues
- [#134](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/134) Fixed issue of attribute deletion not propagated at transfers for dimension-level attributes
- [#144](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/144) Enabled any length of non-coded attributes when limit is over 4000; Correction of link URL in maximum attribute length related messages
- [#258](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/258) Added updatedAfter parameter in transfer service for dataspace transfers
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/277) SDMX-XML v2.0 readers support deletion action features
- [#300](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/300) Implemented storage of updatedAfter information for referential metadata
- [#313](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/313) Updated nuget references to match with ESTAT v8.13.0
- [#314](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/314) Fix null reference bug in case of dataflow (related to a DSD) without attributes
- [#325](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/325) Updated nuget references to match with ESTAT v8.15.0
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/331) Updated nuget references to match with ESTAT v8.15.1
- [#335](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/335) Changed sdmx file reader to use function that does not use the stream
- [#352](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/352) Updated metadata delete and merge actions
- [#355](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/355) Updated nuget references to match with ESTAT v8.16.0
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/361) Updated nuget references to match with ESTAT v8.17.0
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/363) Upgraded migration criteria to update datasets and mappingsets; Fixed mappingset time_pre_formated bug for the very first time a dataflow is initialized
- [#365](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/365) Changed validators to scoped
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS
- [#427](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/427) Culture specific csv import; alpine globalization fix
- [#434](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/434) Coded ref. metadata treated as non-coded
- [#446](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/446) Enhanced intentionally missing feature
- [#451](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/451) Added support for updatedAfter in sourceQuery param of the transfer/dataflow method
- [#453](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/453) Fixed issue with (re)initialisation of dataflow
- [#455](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/455) Fix no. of processed ref. metadata keys at imports with basic validation
- [#460](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/460) Improved actual content constraints and mappingsets management
- [#464](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/464) Fixed init/dataflow response message separate the reason and the outcome 
- [#465](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/465) Fixed FATAL error in data transfer when there are no dataset attributes
- [#466](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/466) Introduced Replace action
- [#469](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/469) Iterative merge per 'Delete' instructions 
- [#470](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/470) Store and retrieve (through updatedAfter) original 'Delete' instructions
- [#471](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/471) Auto-delete fully empty observations, attributes at partial keys and ref. metadata
- [#478](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/478) Updated GCP auth in Gitlab pipeline
- [#480](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/480) Added application of Kestrel settings from configuration; Documentation updated with description of changing max file size
- [#484](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/484) Added missing time dimension check for metadata import
- [#488](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/488) MappingStore connection string config retrieved through IOC instead of configuration file
- [#490](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/490) Aligned Replace with Merge for higher-level attributes and refactored SQL merge logic
- [#492](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/492) Fixed the issue of transfers failing when related DSD has no dimension/group attributes or when DSD has MSD reference
- [#493](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/493) Fixed the bug in log message construction at ref. metadata import with advanced validation
- [#497](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/497) Fixed the issue of deleting time-series attr. also deletes group attributes
- [#499](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/499) Optimized the sdmx file reader logic
- [#501](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/501) Fixed the issue of replacing the value of a Dataflow level attribute deletes all content
- [#502](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/502) Metadata time wildcard fix
- [#504](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/504) Timestamp is not updated and ACC is not recalculated when there is no impact on the stored values
- [#505](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/505) Fixed the bug of deleting higher level attributes did not delete them but skip them
- [#506](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/506) Wildcard deletions also respect the allowed content constraint
- [#508](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/508) Metadata validation updates
- [#510](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/510) Harmonized datetime treatment by using only .net system time in all code parts (and not SQL server time)
- [#511](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/511) Fixed dataflow attribute delete
- [#513](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/513) Added ignore tag for DotStat.Transfer.Excel to exclude it from the unit test code coverage
- [#514](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/514) Fixes applied on transferring metadata
- [#515](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/515) Changed the default action to Merge for SDMX-CSV 1.0 
- [#517](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/517) Transfer of filtered obs. values must include attributes attached at all higher levels
- [#518](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/518) The LAST_UPDATED column of updatedAfter feature is stored as UTC time; Fixed log warning message for sourcequery not collected by appender
- [#523](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/523) Version of Swashbuckle.AspNetCore reverted to v5.6.3 in order to restore missing description on UI
- [#536](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/536) Switched-off metadata dim values returned as a special character instead of null (empty string) 
- [#537](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/537) Improved replace part of transfer data/metadata accross spaces
- [#541](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/541) Updated metadata dim validation
- [#542](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/542) MSD info is populated from structure db when not returned from the file readers
- [#543](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/543) Fixed transfer of group/dimension attributes in case there are no observations in source data space
- [#546](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/546) Change default ref. metadata length from 4000 to MAX in the storage type
- [#548](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/548) Enhanced error message for duplicated observations
- [#550](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/550) Base docker image changed from debian to alpine; Upgrade of google log4net lib as the old one was failing in alpine linux 
- [#552](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/552) Changed the special value to indicate switched-off dimension from "-" to "~"
- [#553](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/553) Changed the values to represent intentionally missing 'NaN' for Float and double and "#N/A" for textual representation.
- [#554](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/554) Fixed bug of mapping set being not created after unsuccessful import
- [#556](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/556) Fixed deleteALL function with basic validation, no summary is returned now
- Added timezone information to health page
- Updated license information
- Removed NUGET_FEED param from dockerfile
- In dockerfile nuget.config is copied prior dotnet restore command 


## v11.1.1

### Database compatibility
- StructureDB v6.20
- DataDB v7.0.1
- CommonDB v3.8

### Issues
- [#126](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/126) Fix for log records of transactions appearing in two different dataspaces
- [#563](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/563) Private repository replaced to Gitlab


## v11.1.0

### Database compatibility
- StructureDB v6.20
- DataDB v7.0.1
- CommonDB v3.8

### Issues
- [#426](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/426) Updated authentication to support ADFS
- [#463](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/463) Fix of issue with importing data from a path (public cloud link) exceeding 2 GB
- [#478](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/478) Updated GCP auth. in devops environments for .NET services


## v11.0.7

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.1
- CommonDB v3.8

### Issues
- [#450](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/450) Fix for non-unique component ID accross DSD & referenced MSD
- [#459](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/459) Applied DatabaseCommandTimeoutInSec (max if multiple dataspaces) to SQL commands run by Eurostat mappingstore libs run by dataaccess/transfer service


## v11.0.6

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.1
- CommonDB v3.8

### Issues
- [#453](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/453) Fix initialising dataflow
- [#444](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/444) Transaction timeout
- [#448](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/448) Primary measure as codelist, bulkinsert convert bug
- [#449](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/449) Fix temp file cleanup


## v11.0.5

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.1
- CommonDB v3.8

### Issues
- [#94](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/94) Script added to DbUp tool to regenerate all DSD views
- [#443](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/443) Fixed issue of files with duplicates being saved skipping duplicated rows with no error message
- [#444](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/444) Fixed time-out issue at data upload of 70+ million observations


## v11.0.4

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#440](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/440) Fixed error of false duplicates reported in advance validation


## v11.0.3

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#439](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/439) Removed temporary log4net internal debug printing that prevented application start on IIS


## v11.0.2

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#286](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/286) TIME_PRE_FORMATED table cleanup fix
- [#289](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/289) References updated to match with NSI WS v8.12.2
- [#429](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/429) Fixed order of reported components to match with the order in destination DSD
- [#435](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/435) Fixed errors with missing referenced dimensions and dataflow attribute management
- [#437](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/437) Fix of index creation applied on staging table in case of large file imports


## v11.0.1

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#430](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/430) Revert the isolation level to READ COMMITTED in connections with no transaction
- [#431](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/431) Fix performance issue with the merge statement from staging to fact table


## v11.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v7.0.0
- CommonDB v3.8

### Issues
- [#91](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/91) Fix multiple contradictory values error at dataset attributes
- [#127](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/127) Introduced data DELETE operation
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) References updated to match with NSI WS v8.12.0
- [#281](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/281) References updated to match with NSI WS v8.12.1
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/285) Allow views to return higher level attribute values when there are no observation level component rows
- [#397](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/397) Implemented MERGE action for CSV v2 data imports
- [#402](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/402) Improved detection of data or metadata content of a CSV v2 file
- [#409](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/409) Fixed transaction id in time-out log message
- [#410](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/410) No fatal error is logged at init/allMappingsets method in case of no DSDs or no dataflows in data space
- [#416](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/416) Fix bug in case there are no dataset attribute values to modify
- [#420](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/420) Fix regression issues in data transfer between data spaces
- [#424](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/424) Fix issue when deleting specific years
- [#425](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/425) Fixed import of first dataset level metadata attribute


## v10.0.5

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#406](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/406) Initialize codeTranslator cache at InitDataDbObjectsOfDataflow method
- [#407](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/407) Add Message-Id header to SMTP request
- [#387](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/387) Add optional SMTP HFrom header configuration
- [#90](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/90) Minor database health check improvements


## v10.0.4

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#403](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/403) Fix creation of actual-CC


## v10.0.3

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#400](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/400) Repair permission test in CleanUpOrphanedCodelists


## v10.0.2

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#6](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/6) Enhance 'DuplicatedRowsInStagingTable' error message
- [#378](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/378) Correct/complement log entry for dsd-cleanup
- [#379](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/379) Fix mail send
- [#382](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/382) Cleanup of orphaned codelists
- [#383](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/383) Fix "already added" in MappingStoreDataAccess, Add missing application name in structdbconnetionString
- [#386](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/386) Avoid re-accessing the same column


## v10.0.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- Disabled resource auto detection of Google logger
- [#380](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/380) Linked application cancellationToken and controller (client) cancellationToken


## v10.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.2.1
- CommonDB v3.8

### Issues
- [#5](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/5) Added gitlab dast scan
- [#29](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/29) Added nuget dependency scanning to CI pipeline
- [#86](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/86) The method cleanup/dsd deletes the mapping sets and actual content constraint of the dataflows
- [#87](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/87) Added connection close fix and isolation level fix
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/245) Mappingset cleanup transaction is executed in dataflow scope
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/251) ESTAT references updated to v8.11.0
- [#267](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/267) Added log4net google appender
- [#277](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/277) Add new logs for import steps and add batchNumber to logs for staging data and metadata
- [#285](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/285) Always set the Actual CC validFrom date
- [#291](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/291) Introduced cancellation tokens and readonly db connections
- [#317](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/317) Fix for performance degradation of dataflow view when adding a value for a dataset-level attribute
- [#324](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/324) Upgraded from .NET Standard 2.1/.NET Corea 3.1 to .NET 6
- [#341](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/341) Added option to pass auth token to httpClient when importing from URL
- [#345](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/345) Fix of missing Swagger descriptions
- [#351](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/351) Import metadata from url
- [#353](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/353) Requeue requests when there is ongoing transactions for the same DSD
- [#361](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/361) Initialize dsd object of dataflows with data management properties
- [#362](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/362) Updated msd cleanup
- [#363](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/363) Fix tryNewTransaction when first request for a dsd is initdataflow
- [#365](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/365) Fix of null reference error at /cleanup/orphans method
- [#369](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/369) Fix of dataset attribute import
- [#373](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/373) Set QUEUED_DATETIME value to EXECUTION_START for transaction rows existed in db before the addition of the field QUEUED_DATETIME
- Added queue details to health check


## v9.1.2

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#269](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/269) Correction of some transfer messages
- [#350](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/350) Change mappingstoredataaccess to scoped add memory cache


## v9.1.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#342](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/342) Fix of queued transactions blocking all other transactions


## v9.1.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.1
- CommonDB v3.8 

### Issues
- [#278](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/278) Mark transactions as aborted or closed 
- [#319](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/319) Import from URL made async 
- [#320](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/320) Bug fix formatting of the email subject and summary 
- [#114](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/114) Support for NaN as observation 
- [#304](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/304) Manage unhandled exceptions #304
- [#331](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/331) fix email wrong action type in email 
- [#147](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/147) add servicebus functionality 
- [#340](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/340) Swagger fix 
- [#182](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/182) Fix of dataset column mapping issue 


## v9.0.1

### Database compatibility
- StructureDB v6.19
- DataDB v6.0
- CommonDB v3.8 

### Issues
- [#321](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/321) Fix for issue of data transactions failing with dataflows supporting ref.metadata


## v9.0.0

### Database compatibility
- StructureDB v6.19
- DataDB v6.0
- CommonDB v3.8 

### Issues
- [#310](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/310) block all transactions when cleanup is in progress, Set v2 of the rest api as default
- [#316](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/316) Pretify error msg when data import from sdmx url is not accessible 
- [#315](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/315) Fix optional parameters 
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix metadataimport dsd with SUPPORT_DATETIME annotation
- [#309](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/309) Add missing transfer-service documentation metadata api parameters
- [#306](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/306) Fix ACC validity update at metadata imports 
- [#164](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/164) Added missing registrations related to MAAPI.NET v8.9.2 changes 
- [#234](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/234) References updated to NSI v8.9.2 
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Documentation updated 
- [#603](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-explorer/-/issues/603) Dataset Attribute parameter passed as NVarchar
- [#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/130) Improvements to firstNObservations and lastNObservations queries 
- [#302](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/302) Enhanced log message for init/dataflow method 
- [#305](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/305) fix Hierarchical Referential Metadata Attributes are not Imported 
- [#224](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/224) References updated to match Eurostat NSI v8.9.1 
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#82](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/82) Fix MetadataDataFlow view period_start and period_end add datetime support 
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/287) Complete swagger documentation 
- [#293](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/293) Add a proper error description when uploading metadata file to a structure without annotation link to MSD
- [#296](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/296) Correct the transfer log messages for Referential Metadata transactions
- [#176](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/176) Add source dataspace and data source fields to transaction logs 
- [#295](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/295) Remove ORDER BY for metadata Mappingsets 
- [#81](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/81) eurostat packages update to 8.9.0
- [#80](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/80) Metadata storage implementation 


## v8.2.0 

### Database compatibility
- StructureDB v6.17
- DataDB v5.6
- CommonDB v3.8 

### Issues
- [#184](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/184) Improved management of time pre formatted entries and deletion of mapping set relates objects from database 
- [#284](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/284) Update creation time of zip entry on extract
- [#228](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/228) DataAccess NuGet references amended 
- [#230](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/230) References updated to NSI v8.8.0 
- [#280](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/280) Analyse issue "Expecting the instance to be stored in singleton scope, but...


## v8.1.0 

### Database compatibility
- StructureDB v6.17
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#39](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/39) Readme appended with description of changing authentication token claims mapping
- [#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/174) References updated to match Eurostat NSI v8.7.1
- [#189](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/189) Manage Time in group attributes - part 2
- [#275](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/275) Fix for zip file extraction not cleaned from temp folder and "filesCount" added to health page

## v8.0.1 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#76](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/76) Errors found in the AllowMissingComponents SQL migration script for the recreation of the DSD/DF views
- [#77](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/77) Errors found in the ChangeFactTableIndexes SQL migration script for the recreation of the Fact table indexes


## v8.0.0 

### Database compatibility
- StructureDB v6.16
- DataDB v5.6
- CommonDB v3.7 

### Issues
- [#33](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-auth-management/-/issues/33) Added TokenUrl param to auth config
- [#75](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/75) Change primary key from PERIOD_SDMX to PERIOD_START and PERIOD_END, fix of semantic error displayed when time period is invalid
- [#113](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/113) Support REPORTING_YEAR_START_DAY attribute, Time range format, date + time format, fixed NumberStyles used at float and double, usage of SUPPORT_DATETIME annotation added
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/161) References updated to match Eurostat NSI v8.5.0
- [#178](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/178) Stream import files
- [#210](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/210) New date format applied on PIT release and restoration dates
- [#212](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/212) Added missing Primary measure representation warning, added logs to returned response for init/dataflow method
- [#214](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/214) Fix of codelist item ids not in consecutive order issue
- [#215](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/215) Allow non reported components
- [#223](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/223) Changed localization text for bulk copy notifications and improved merge performance with full details
- [#226](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/226) Added number of obs to pre-generated actual content constraints
- [#231](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/231) Fill order_by information for mappingsets, recreate DSD and DataFlow views include SID column
- [#236](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/236) Fixes for file validations
- [#237](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/237) Updated mapping set creation function to use new TIME_PRE_FORMATED mapping
- [#242](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/242) New ApplyEmptyDataDbConnectionStringInMSDB configuration parameter added
- [#245](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/245) Fix of column mapping during data transfer; fix missing attributes in destination during transfer
- [#251](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/251) Corrected the creation of staging table, use fact table info to create time columns, when the fact table exists
- Added added 'disk size' and 'disk space free %' to health check


## v7.2.0 
### Description

### Issues
[#72](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/72) Added support for creation of read-only user to DbUp
[#116](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/116) Added support for Azure SQL Database and Azure SQL Managed Instance to DbUp


## v7.1.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.2 
- CommonDB v3.7 

### Issues
[#130](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/130) added temp file cleaning service
[#64](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/64) Skip codes in allowed constraint not part of codelist 
[#44](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/44) Generate empty ACC for live version when first upload targets PIT 
[#170](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/170) remove the requirement that the action can only be performed if the DSD is already deleted from the Mapping Store DB for method /cleanup/dsd  
[#70](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/70) Password change added to DbUp 
[#71](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/71) sql server compatible issues 
[#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) adjusted attribute validation message for edd import
[#132](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) Allow imports with basic and full validations. New validation only functions 
[#174](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/174) Create transaction item for init/allMappingsets method 
[#201](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/201) Fix of transfer between two different data versions 
[#4](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/4) Update Transfer service error messages 
[#123](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/123) remove primary key in staging table 
[#198](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/198) Initialize empty mappingsets and actual content constraints for the first dataload 
[#205](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/205) Fix bug, actual content constraint not updated after data transfer 
[#202](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/202) Fix of wrong content constraint start date update issue 


## v7.0.0 

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues
[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Bugfix, 4.0 Time dimension without codelist.

## v6.3.0 
### Description

### Issues
[#69](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/69) Bugfix for issue, dbup changes target db to single user mode at start and sets back to multiuser on finishs. 

## v6.1.0 
### Description

### Database compatibility
- StructureDB v6.12 
- DataDB v5.0 
- CommonDB v3.6 

### Issues

[#1](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-quality-assurance/-/issues/1) Remove obsolete/unused ci variables. 
<BR>[#96](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/96) Cleanup mappingsets updated. 
<BR>[#168](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/168) Fix codelist mapping identification of DSD components. 
<BR>[#60](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/60) Csv reader validation error messages.
<BR>[#124](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/124) Non-numeric measure type implementation related changes. 
<BR>[#67](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/67) Add ExecutionTimeout in DbUp.
<BR>[#159](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/159) Manage Time in group attribute.
<BR>[#50](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/50) Storage of non-observation attributes at series and observation level.
<BR>[#51](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/51) Performance improvement   Remove ROW_ID from DSDs with less than 34 dimensions.
<BR>[#84](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/84) Support for DSD without Time dimension. 
<BR>[#16](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/16) Time dimension storage without codelist. 
<BR>[#57](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/57) Change producers to report all non-dataset attributes at obs level.
<BR>[#167](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/167) Validation of data database version.


## v6.0.5 (MSDB v6.12, DataDB v3.5) 
### Description

`WARNING`
Note that if the Data database is in a corrupted state (missing tables) before the migration to a new version, the migration tool (dbup tool) can potentially fail to update the database. We recommend the following steps during the migration:
1.  Backup all databases
2. When the dbup tool is run, please look carefully in the result of the execution (It will prompt you if there were issues during the update).
     - One known issue is that it will skip the creation of internal views when the required tables are not found.
3. If in the previous steps, there are errors for specific DSDs/Dataflows, **Before you do any other action, first use the method */init/dataflow* for the failing dataflows.**
     - If the previous doesn't help/work then the you must delete these dsd/dfs and run the */cleanup/dsd* function for those.
--------
After a successful migration, **Before any other action** you must first run the function */init/allMappingSets* for all the configured data spaces.

### Issues
- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.


## v6.0.0 (MSDB v6.12, DataDB v3.5) 
### Description

==Important== Before using this software, sure to have backed up databases.
This version is a major update with breaking changes. 

This release contains upgrades of the Eurostat nuget package(s), the changelog for this can be found [here](https://gitlab.com/sis-cc/eurostat-sdmx-ri/maapi.net.mirrored/-/blob/master/CHANGELOG.md)

- [#173](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/173) Added support to get logs of requests not creating transaction entry in db via status request method.
- [#103](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/103) References updated to Eurostat NSI v8.1.2.
- [#110](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/110) Add feature to validate the allowed content constraint for coded-attributes transfer-service.
- [#160](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/160) Fix issues with Mappingsets initialisation in transfer service.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/63) Bugfix for issue Exception of type 'DotStat.Db.Exception.KeyValueReadException' in Excel+EDD upload response.
- [#N/A]() Increase the threshold of the performance tests for small imports.
- [#N/A]() Update the documentation of the PIT restoration function.
- [#165](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/165) Bugfix allDataflows method fails with a Timed Out Error.
- [#161](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/161) Bugfix dataflow views with non mandatory dsd lvl attributes.
- [#120](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/120) Feature to consult the status of the data imports/transactions.
- [#49](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/49) Automatically create mapping sets in the mapping store database.
- [#154](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/154) For transfer, check existence of target dataflow and necessary permissions before responding with transaction ID.
- [#157](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/157) Validate when mappingset valid to date is datemax, set it to datemax - 1 second.
- [#63](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/63) remove check if dataflow already exists on /init/dataflow method, 
- [#N/A]() Added a SourceVersion to the Transfer endpoint. Involved adding an intermediate param, IFromSqlTransferParam, as ISqlTransferParam is used for Sdmx file to Sql as well.
- [#149](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/149) Bugfix in excel upload .
- [#108](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-common/-/issues/108) Use Estat PermissionType .
- [#N/A](https://gitlab.com/sis-cc/.stat-suite/keycloak/-/issues/7) Change from implicit flow to Authorization code flow.
- [#143](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-data-lifecycle-manager/-/issues/143) Detailed import summary added.
- [#48](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/48) Automatically create data database sql views for dsd and dataflows.
- [#53](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/53) Grant permision to create view to dotstatwriter role.
- [#105](https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/105) Use dataReaderManager, fix double unzip.


## 5.0.0 (MSDB v6.9, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/93
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/83
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/79
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/95
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/29
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/78
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/94
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-config/-/issues/1
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/96
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/106
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/111
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-data-access/-/issues/46
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/117
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/100
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-ws/-/issues/49
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/107
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/101
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/102

## v4.2.4 2020-04-18 (MSDB v6.7, DataDB v2.1)
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.2.3 2020-04-18 (MSDB v6.7, DataDB v2.1)

- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/97
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/47
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/84
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/3
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/62
- https://gitlab.com/sis-cc/dotstatsuite-documentation/-/issues/65
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/58
- https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/65


## v4.1.2 2020-03-30 (MSDB v6.7, DataDB v2.1)

https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-sdmxri-nsi-plugin/-/issues/42
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/54
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/75
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/71
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/80
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/88
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/85
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/77
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/86
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/76
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/82
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/91
https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/issues/92


##  v4.0.3 2020-01-29 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with changes to the authentication management.

- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/66
- /sis-cc/.stat-suite/dotstatsuite-core-transfer/issues/52


##  v3.0.1 2020-01-22 (MSDB v6.7, DataDB v2.1)
This release contains breaking changes with a new entry in the dataspaces.private.json and the introduction of localization.json via the Dotstat.config nuget package.     

- sis-cc/.stat-suite/dotstatsuite-core-common/issues/102