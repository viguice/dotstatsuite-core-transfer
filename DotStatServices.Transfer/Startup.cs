using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IdentityModel.Tokens.Jwt;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using DotStat.Common.Configuration;
using DotStatServices.Transfer.HealthCheck;
using DotStat.Common.Logger;
using DotStat.Transfer.Messaging.Consumer;
using DotStat.Db.Service;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using DotStatServices.Transfer.Utilities;
using DryIoc;
using DryIoc.Microsoft.DependencyInjection;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Cors.Infrastructure;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Authorization;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Logging;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using Newtonsoft.Json.Serialization;

namespace DotStatServices.Transfer
{
    [ExcludeFromCodeCoverage]
    public class Startup
    {
        public static readonly DateTime Start;
        private readonly AuthConfiguration _auth;
        private readonly BaseConfiguration _baseConfig;

        static Startup()
        {
            Start = DateTime.Now;
        }

        public Startup(IConfiguration configuration)
        {
            _baseConfig = configuration.Get<BaseConfiguration>();
            _auth = configuration.GetSection("auth").Get<AuthConfiguration>();
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            IdentityModelEventSource.ShowPII = true;

            // By default, Microsoft has some legacy claim mapping that converts standard JWT claims into proprietary ones.
            // This removes those mappings.
            JwtSecurityTokenHandler.DefaultInboundClaimTypeMap.Clear();

            var maxConcurrentTransactions = 1;
            if (_baseConfig?.MaxConcurrentTransactions != null)
                maxConcurrentTransactions = _baseConfig.MaxConcurrentTransactions;

            BackgroundTask.Register(services, maxConcurrentTransactions);
            TempFileManager.RegisterService(services);
            services.AddSingleton<IHostedService, ConsumerService>();

            services.AddHttpClient();

            services
                .AddMvc(options =>
                {
                    if (_auth?.Enabled == true)
                    {
                        options.Filters.Add(new AuthorizeFilter());
                    }
                    options.Filters.Add(new ProducesAttribute("application/json"));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.OK));
                    options.Filters.Add(new ProducesResponseTypeAttribute(typeof(OperationResult), (int) HttpStatusCode.BadRequest));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Unauthorized));
                    options.Filters.Add(new ProducesResponseTypeAttribute((int) HttpStatusCode.Forbidden));
                })
                .AddNewtonsoftJson(options =>
                {
                    options.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                });


            services.AddApiVersioning(options =>
            {
                options.AssumeDefaultVersionWhenUnspecified = true;
                options.ReportApiVersions = true;
            });


            services.AddVersionedApiExplorer(options =>
            {
                // add the versioned api explorer, which also adds IApiVersionDescriptionProvider service  
                // note: the specified format code will format the version as "'v'major[.minor][-status]"  
                options.GroupNameFormat = "'v'VVV";

                // note: this option is only necessary when versioning by url segment. the SubstitutionFormat  
                // can also be used to control the format of the API version in route templates  
                options.SubstituteApiVersionInUrl = true;
            });


            // -----------------------------------------------------------------------

            if (_auth?.Enabled == true)
            {
                var validAudiences = new List<string>(2)
                {
                    _auth.ClientId
                };

                if (!string.IsNullOrEmpty(_auth.Audience))
                {
                    validAudiences.Add(_auth.Audience);
                }

                services.AddAuthentication(options =>
                {
                    options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                    options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;

                }).AddJwtBearer(options =>
                {
                    options.Authority = _auth.Authority;
                    options.RequireHttpsMetadata = _auth.RequireHttps;
                    options.TokenValidationParameters = new TokenValidationParameters()
                    {
                        ValidateIssuer = _auth.ValidateIssuer,
                        ValidAudiences = validAudiences
                    };
                });
            }

            // -----------------------------------------------------------------------

            //Allow HttpContext to be accessed by external library (log4net logHelper.cs)
            services.AddHttpContextAccessor();


            //This line adds Swagger generation services to our container.
            services.AddSwaggerGen(c =>
            {
                const string title = "Transfer service";
                const string description = $@"The .Stat Suite Core Transfer service provides a .Stat Core data storage API for: 
                    <ul>
                        <li>Upload of SDMX data and/or referential metadata files (csv, xml) into a .Stat Core data storage.</li>
                        <li>Upload of Excel data and/or referential metadata files (using a specific data mapping definition) into a .Stat Core data storage.</li>
                        <li>Transfer of data and/or referential metadata between two .Stat Core data storages.</li>
                    </ul>
                    <br>See the <a href='https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/tree/master'>full API documentation</a> and the <a href='https://sis-cc.gitlab.io/dotstatsuite-documentation/using-api/'>functional specifications of the .Stat Suite APIs</a>.";
                
                var license = new OpenApiLicense()
                {
                    Name = "MIT License",
                    Url = new Uri(
                        "https://gitlab.com/sis-cc/.stat-suite/dotstatsuite-core-transfer/-/blob/master/LICENSE")
                };

                c.SwaggerDoc(
                    "v1.2",
                    new OpenApiInfo
                    {
                        Title = title,
                        Version = "1.2",
                        Description = description,
                        License = license
                    });

                c.SwaggerDoc(
                    "v2",
                    new OpenApiInfo
                    {
                        Title = title,
                        Version = "2",
                        Description = description,
                        License = license
                    });


                c.SwaggerDoc(
                    "v3",
                    new OpenApiInfo
                    {
                        Title = title,
                        Version = "3",
                        Description = description,
                        License = license
                    });

                if (_auth?.Enabled == true)
                {
                    var securityScheme = new OpenApiSecurityScheme()
                    {
                        Type = SecuritySchemeType.OAuth2,
                        In = ParameterLocation.Header,
                        Flows = new OpenApiOAuthFlows()
                        {
                            AuthorizationCode = new OpenApiOAuthFlow()
                            {
                                Scopes = _auth.Scopes.ToDictionary(x => x, x => x),
                                AuthorizationUrl = new Uri(_auth.AuthorizationUrl),
                                TokenUrl = new Uri(string.IsNullOrEmpty(_auth.TokenUrl) ? _auth.AuthorizationUrl.Replace("openid-connect/auth", "openid-connect/token") : _auth.TokenUrl)
                            }
                        }
                    };
                        
                    c.AddSecurityDefinition("oauth2", securityScheme);

                    c.AddSecurityRequirement(new OpenApiSecurityRequirement()
                    {
                        {
                            new OpenApiSecurityScheme()
                            {
                                Reference = new OpenApiReference
                                {
                                    Type = ReferenceType.SecurityScheme,
                                    Id = "oauth2",
                                }
                            },
                            _auth.Scopes.ToList()
                        }
                    });
                }

                // Use the xml files generated by ASP.NET for DotStat.Transfer and DotStatServices.Transfer projects to get the xml comments.
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, $"{Assembly.GetExecutingAssembly().GetName().Name}.xml"));
                c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, "DotStat.Transfer.xml"));
                c.UseInlineDefinitionsForEnums();
                c.OperationFilter<SwaggerFileOperationFilter>();
            });

            //https://docs.microsoft.com/en-us/aspnet/core/host-and-deploy/health-checks?view=aspnetcore-3.1
            services.AddHealthChecks()
                .AddCheck<ServiceHealthCheck>("service", tags: new[] {"live"})
                .AddCheck<DbHealthCheck>("database")
                .AddCheck<MemoryHealthCheck>("memory")
                .AddCheck<DiskSizeHealthCheck>("disk")
                .AddCheck<QueueHealthCheck>("queue");



            services.AddCors();
            services.AddHostedService<TransactionCleanupHostedService>();

            // Dry IOC setup
            return new Container(
                    rules =>
                        rules.With(FactoryMethod.ConstructorWithResolvableArguments))
                .WithDependencyInjectionAdapter(services)
                .ConfigureServiceProvider<ApiIoc>();
            //.WithoutThrowOnRegisteringDisposableTransient())
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            // Global exception handler
            var exceptionMiddleware = new JsonExceptionMiddleware(env);
            app.UseExceptionHandler(new ExceptionHandlerOptions
            {
                ExceptionHandler = exceptionMiddleware.Invoke
            });

            app.UseCors(BuildCorsPolicy);
            app.UseHealthChecks("/health", new DotStatHealthCheckOptions());
            app.UseHealthChecks("/live", new DotStatHealthCheckOptions()
            {
                Predicate = (x) => x.Tags.Contains("live")
            });

            app.UseMiddleware<CompatibilityCheckMiddleware>();

            if (_auth?.Enabled == true)
            {
                app.UseAuthentication();
            }

            Log.Configure(app.ApplicationServices.GetRequiredService<IHttpContextAccessor>());

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("v3/swagger.json", "Transfer service v3");
                c.SwaggerEndpoint("v2/swagger.json", "Transfer service v2");
                c.SwaggerEndpoint("v1.2/swagger.json", "Transfer service v1.2");

                if (_auth?.Enabled == true)
                {
                    c.OAuthClientId(_auth.ClientId);
                    c.OAuthUsePkce();
                }
            });


            app.UseRouting();
            app.UseEndpoints(endpoints => { endpoints.MapControllers(); });
        }
        private void BuildCorsPolicy(CorsPolicyBuilder builder)
        {
            var allowedOrigins = _baseConfig?.CorsAllowedOrigins;

            // Handle empty allowedOrigins by setting appropriate policy
            if (allowedOrigins == null || !allowedOrigins.Any())
            {
                builder.SetIsOriginAllowed(d => true);
            }
            else
            {
                builder.WithOrigins(allowedOrigins);
            }

            // Allow any header, method, and credentials (regardless of allowed origins)
            builder.AllowAnyHeader()
                   .AllowAnyMethod()
                   .AllowCredentials();
        }
    }
}