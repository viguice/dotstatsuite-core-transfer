using System;
using System.Collections.Generic;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using DotStat.Common.Auth;
using DotStat.Common.Exceptions;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using DotStat.Domain;
using DotStat.Transfer.Utils;
using DotStatServices.Transfer.Model;
using System.Threading;
using System.Threading.Tasks;
using Extensions = DotStat.Transfer.Utils.Extensions;
using Estat.Sdmxsource.Extension.Constant;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// Status controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiVersion("3")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class StatusController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor; 
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly BaseConfiguration _configuration;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public StatusController(
            IHttpContextAccessor contextAccessor,
            IAuthorizationManagement authorizationManagement,
            IAuthConfiguration authConfiguration,
            BaseConfiguration configuration,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver
        )
        {
            _contextAccessor = contextAccessor;
            _authorizationManagement = authorizationManagement;
            _authConfiguration = authConfiguration;
            _configuration = configuration;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        /// <summary>
        /// Get the transaction status information by transaction ID, for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace to search for the request. <br></br> Example: <code>design</code></param>
        /// <param name="id">The transaction ID, received during the submission of the request.<br></br> Example: <code>1</code>,<code>155</code></param>
        /// <remarks>
        /// Example: Get the status of the request ID #4 in the 'design' dataspace
        /// 
        ///     POST /status/request
        ///     {
        ///        "dataspace": "design",
        ///        "id": 4
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Information successfully retrieved</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not found</response>
        [HttpPost]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Route("status/request")]
        public async Task<ActionResult<ImportSummary>> StatusById(
            [FromForm, Required] string dataspace,
            [FromForm, Required] int id,
            CancellationToken cancellationToken
        )
        {
            try
            {
                var userPrincipal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                var space = dataspace.GetSpaceInternal(_configuration, "en");

                var unitOfWork = _unitOfWorkResolver(space.Id);

                var transaction = await unitOfWork.TransactionRepository.GetTransactionById(id, cancellationToken, true);

                var logs = unitOfWork.TransactionRepository.GetTransactionLogs(id, _authorizationManagement.IsAuthorized(userPrincipal, PermissionType.AdminRole), cancellationToken);

                var transactionLogs = await logs.ToArrayAsync(cancellationToken);

                // When there is no such transaction record then it was an initialization transaction, only logs will be in the database
                if (transaction == null)
                {
                    // If there are even no logs then there is no info about this transaction in db
                    if (!transactionLogs.Any())
                    {
                        return new NotFoundResult();
                    }

                    // Return summary with default values of info not present
                    return new ImportSummary()
                    {
                        SourceDataspace = "N/A",
                        DestinationDataspace = dataspace,
                        RequestId = id,
                        Action = TransactionType.Unknown.ToString(),
                        SubmissionTime = null,
                        ExecutionStart = null,
                        ExecutionEnd = null,
                        UserEmail = "N/A",
                        Artefact = "N/A",
                        SourceData = "N/A",
                        ExecutionStatus = "N/A",
                        Outcome = "N/A",
                        Logs = transactionLogs,
                        LastUpdated = null
                    };
                }

                var outcome = Extensions.GetTransactionOutcome(transaction.Status, transactionLogs);

                return new ImportSummary()
                {
                    SourceDataspace = transaction.SourceDataspace,
                    DestinationDataspace = dataspace,
                    RequestId = id,
                    Action = transaction.Type.ToString(),
                    SubmissionTime = transaction.QueuedDateTimeAsString,
                    ExecutionStart = transaction.ExecutionStartAsString,
                    ExecutionEnd = transaction.ExecutionEndAsString,
                    UserEmail = transaction.UserEmail,
                    Artefact = transaction.ArtefactFullId,
                    SourceData = transaction.DataSource,
                    ExecutionStatus = transaction.Status.ToString(),
                    Outcome = outcome,
                    Logs = transactionLogs,
                    LastUpdated = transaction.LastUpdatedAsString
                };

            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception e)
            {
                Log.Error(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurredNoId));
                Log.Fatal(e);
                throw;
            }
        }
        /// <summary>
        /// Search for transactions and their logs in a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace to search for the requests. <br></br> Example: <code>design</code></param>
        /// <param name="userEmail">Optional - Filter requests by user email, default (any). <br></br> Example: <code>email@domain.com</code></param>
        /// <param name="artefact">Optional - Filter requests by SDMX ID artefact, default (any). <br></br>Format: AGENCYID:ARTEFACTID(VERSION)<br></br> Example: <code>OECD:MEI(1.0)</code>, <code>OECD:DF_MEI(1.0)</code></param>
        /// <param name="submissionStart">Optional - Filter requests by the start Datetime when the import/transfer was requested<br></br> default (24Hrs before now) <br></br> Format: (dd-MM-yyyy HH:mm:ss or YYYY-MM-DDThh:mm:ssTZD)<br></br> Example: <code>04-06-2022 10:16:01</code>, <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code></param>
        /// <param name="submissionEnd">Optional - Filter requests by the end Datetime when the import/transfer finished processing<br></br> default (now)<br></br> Format: (dd-MM-yyyy HH:mm:ss or YYYY-MM-DDThh:mm:ssTZD)<br></br> Example: <code>04-06-2022 10:16:01</code>, <code>2022-06-04T10:16:01</code>, <code>2022-06-04T08:16:01Z</code>, <code>2022-06-04T10:16:01+02:00</code>, <code>2022-06-04T10:16:01.021+02:00</code></param>
        /// <param name="executionStatus">Optional - Filter requests by its current Status <br></br>Possible values:  Queued (0), InProgress (1), Completed (2), TimedOut (3), Canceled (4), default (any)</param>
        /// <param name="executionOutcome">Optional - Filter requests by its final outcome <br></br>Possible values: Success (0), Warning (1), Error (2), None (3), default (any)</param>
        /// <remarks>
        /// Example: Get the status of the requests in the 'design' dataspace, for the user email 'email@domain.com' and completed with errors 
        /// 
        ///     POST /status/requests
        ///     {
        ///        "dataspace": "design",
        ///        "userEmail": "email@domain.com",
        ///        "executionOutcome": 2
        ///     }
        ///     
        /// </remarks>
        /// <returns></returns>
        /// <response code="200">Information successfully retrieved</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="401">Unauthorized</response>
        /// <response code="404">Not found</response>
        [HttpPost]
        [Route("status/requests")]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        public async Task<ActionResult<List<ImportSummary>>> QueryRequests(
            [FromForm, Required] string dataspace,
            [FromForm] string userEmail,
            [FromForm] string artefact,
            [FromForm] string submissionStart,
            [FromForm] string submissionEnd,
            [FromForm] ExecutionStatus? executionStatus,
            [FromForm] TransactionOutcome? executionOutcome,
            CancellationToken cancellationToken
        )
        {
            try
            {
                var userPrincipal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping);

                var language = string.IsNullOrEmpty(_configuration.DefaultLanguageCode)
                ? "en" : _configuration.DefaultLanguageCode;

                var space = dataspace.GetSpaceInternal(_configuration, language);

                var unitOfWork = _unitOfWorkResolver(space.Id);

                if (!string.IsNullOrEmpty(userEmail))
                    userEmail.IsValidEmail(true);

                var artefactFullId = "";
                if (!string.IsNullOrEmpty(artefact))
                    artefactFullId = artefact.GetArtefact(language).ToString();

                var submissionStartDateTime = submissionStart.GetDateTimeInUtc(nameof(submissionStart), language);
                var submissionEndDateTime = submissionEnd.GetDateTimeInUtc(nameof(submissionEnd), language);
                //Default 24 hrs before now
                submissionStartDateTime ??= DateTime.UtcNow.AddDays(-1);
                //Default now
                submissionEndDateTime ??= DateTime.UtcNow;
                //Validate submissionEnd > submissionStart
                if(submissionEndDateTime <= submissionStartDateTime)
                    throw new DotStatException(string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.EndDateBeforeStart, language),
                        submissionEndDateTime, submissionStartDateTime));

                //Filter on status
                var filterStatus = new TransactionStatus();
                Enum.TryParse(executionStatus?.ToString(), out filterStatus);

                var transactions = unitOfWork.TransactionRepository.GetTransactions(userEmail,
                    artefactFullId,
                    (DateTime)submissionStartDateTime,
                    (DateTime)submissionEndDateTime,
                    executionStatus == null ? null : new TransactionStatus?(filterStatus),
                    cancellationToken,
                    true);

                var result = new List<ImportSummary>();
                await foreach (var transaction in transactions)
                {
                    var logs = unitOfWork.TransactionRepository.GetTransactionLogs(transaction.Id, _authorizationManagement.IsAuthorized(userPrincipal, PermissionType.AdminRole), cancellationToken, true);
                    var transactionLogs = await logs.ToArrayAsync(cancellationToken);
                    var outcome = Extensions.GetTransactionOutcome(transaction.Status, transactionLogs);

                    //filter by outcome
                    if (executionOutcome != null && outcome != executionOutcome.ToString()) continue;

                    result.Add(new ImportSummary()
                    {
                        SourceDataspace = transaction.SourceDataspace,
                        DestinationDataspace = dataspace,
                        RequestId = transaction.Id,
                        Action = transaction.Type.ToString(),
                        SubmissionTime = transaction.QueuedDateTimeAsString, 
                        ExecutionStart = transaction.ExecutionStartAsString,
                        ExecutionEnd = transaction.ExecutionEndAsString,
                        Artefact = transaction.ArtefactFullId,
                        SourceData = transaction.DataSource,
                        UserEmail = transaction.UserEmail,
                        ExecutionStatus = transaction.Status.ToString(),
                        Outcome = outcome,
                        Logs = transactionLogs,
                        LastUpdated = transaction.LastUpdatedAsString
                    });
                    
                }

                if(result.Count==0)
                    return new NotFoundResult();

                return result;
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception e)
            {
                Log.Error(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurredNoId));
                Log.Fatal(e);
                throw;
            }
        }
    }
}
