using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Domain;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Threading.Tasks;
using DotStat.Common.Enums;
using DotStat.Db.Dto;
using DotStat.Transfer.Exception;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Param;
using DotStatServices.Transfer.BackgroundJob;
using System.Threading;
using DotStat.Common.Exceptions;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Utils;
using Estat.Sdmxsource.Extension.Constant;
using DotStat.DB.Repository;
using DotStat.Db.Service;
using static DotStat.Common.Localization.LocalizationRepository;

namespace DotStatServices.Transfer.Controllers
{
    /// <summary>
    /// CleanUp controller
    /// </summary>
    [ApiVersion("1.2")]
    [ApiVersion("2")]
    [ApiVersion("3")]
    [ApiController]
    [Route("{apiVersion:apiVersion}/")]
    public class CleanUpController : ControllerBase
    {
        private readonly IHttpContextAccessor _contextAccessor;
        private readonly IAuthConfiguration _authConfiguration;
        private readonly BaseConfiguration _configuration;
        private readonly CommonManager _commonManager;

        private readonly BackgroundQueue _backgroundQueue;
        private readonly IMailService _mailService;
        private readonly IAuthorizationManagement _authorizationManagement;
        private readonly UnitOfWorkResolver _unitOfWorkResolver;
        private readonly DotStatDbServiceResolver _dotStatDbServiceResolver;

        /// <summary>
        /// Constructor for this controller filled by DryIoc
        /// </summary>
        public CleanUpController(
            IHttpContextAccessor contextAccessor,
            IAuthConfiguration authConfiguration,
            BaseConfiguration configuration,
            CommonManager commonManager,
            BackgroundQueue backgroundQueue,
            IMailService mailService,
            IAuthorizationManagement authorizationManagement,
            UnitOfWorkResolver unitOfWorkResolver,
            DotStatDbServiceResolver dotStatDbServiceResolver)
        {
            _contextAccessor = contextAccessor;
            _authConfiguration = authConfiguration;
            _configuration = configuration;
            _commonManager = commonManager;
            _backgroundQueue = backgroundQueue;
            _mailService = mailService;
            _authorizationManagement = authorizationManagement;
            _unitOfWorkResolver = unitOfWorkResolver;
            _dotStatDbServiceResolver = dotStatDbServiceResolver;
        }

        /// <summary>
        /// Delete all Data DB entries of orphan DSDs and its linked MSDs in the data database for a given dataspace
        /// </summary>
        /// <param name="dataspace"> The ID of the dataspace where the orphan DSDs should be removed. <br></br> Example: <code>design</code></param>
        /// <param name="sendEmail"> Optional - Indicate when the service should send email. <br></br>Possible values: Always (0), Never (1), IfError (2), default (Always)</param>
        /// <remarks>
        /// Example: Delete all orphan DSDs and its linked MSDs in the dataspace 'design'
        /// 
        ///     DELETE /cleanup/orphans
        ///     {
        ///        "dataspace": "design"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction for a DSD</response>
        /// <response code="207">Multi-Status, containing http status codes 200 and 409</response> 
        [HttpDelete]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Route("cleanup/orphans")]
        public async Task<ActionResult<OperationResult>> CleanUpOrphanDsdsV2(
            [FromForm, Required] string dataspace,
            [FromForm] SendEmailOptionsEnum sendEmail = SendEmailOptionsEnum.Always 
            )
        {
            var cancellationToken = CancellationToken.None;
            Log.Debug($"Request {Request?.Method}: {Request?.Path.Value}");

            var dataSpace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode);

            var dotStatDbService = _dotStatDbServiceResolver(dataspace);
            var unitOfWork = _unitOfWorkResolver(dataspace);

            var transactionId = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            var transferParam = new TransferParam()
            {
                Id = transactionId,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataSpace,
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };
        
            var transactionResult = new TransactionResult()
            {
                TransactionType = TransactionType.CleanupOrphans,
                TransactionId = transactionId,
                Aftefact = SpecialTransactionArtefactIds.All,
                DestinationDataSpaceId = dataSpace.Id, 
                User = transferParam.Principal.Email,
                TransactionStatus = TransactionStatus.Queued
            };
            
            //Set appender to log to db
            LogHelper.RecordNewTransaction(transactionId, dataSpace);

            var message = string.Format(
                LocalizationRepository.GetLocalisedResource(Localization.ResourceId.SubmissionResult, transferParam.CultureInfo.TwoLetterISOLanguageName),
                transactionId);

            Log.Notice(message);

            var transaction = await unitOfWork.TransactionRepository.CreateTransactionItem(
                transferParam.Id, SpecialTransactionArtefactIds.All, transferParam.Principal,
                sourceDataSpace: null, dataSource: null, TransactionType.CleanupOrphans, 
                requestedTargetVersion: null, _configuration.ServiceId, cancellationToken, true);

            //Function with the main steps to execute the request
            async Task mainTask(CancellationToken backgroundCancellationToken)
            {
                using var internalCancellationToken = backgroundCancellationToken.SetBackgroundTaskCompletionSteps(dotStatDbService, transferParam, cancellationToken);
                try
                {
                    await _commonManager.CleanUpAllOrphans(transferParam, transaction, internalCancellationToken.Token);
                }
                catch (ConcurrentTransactionException)
                {
                    // Transaction creation attempt failed due to an ongoing concurrent transaction
                    Log.Notice(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorConcurrentTransactionV2));
                }
                catch (DotStatException exception)
                {
                    Log.Warn(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorV2));
                    Log.Error(exception);
                }
                catch (Exception e)
                {
                    if (e is not TaskCanceledException && !internalCancellationToken.Token.IsCancellationRequested)
                    { 
                        Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                        Log.Fatal(e);
                    }
                }
                finally
                {
                    transactionResult.TransactionStatus = await dotStatDbService.GetFinalTransactionStatus(transferParam.Id, CancellationToken.None);
                    await _mailService.SendMail(
                        transactionResult,
                        transferParam.CultureInfo.TwoLetterISOLanguageName,
                        _authorizationManagement.IsAuthorized(transferParam.Principal, PermissionType.AdminRole),
                        sendEmail
                    );
                }

                await Task.CompletedTask;
            }

            //Delegate to check if this request can begin to process, or if it should be placed back to the queue.
            async Task<bool> canBeProcessed(CancellationToken backgroundCancellationToken)
            {
                Log.SetTransactionId(transferParam.Id);
                Log.SetDataspaceId(transferParam.DestinationDataspace.Id);
                return await dotStatDbService.TryNewTransactionWithNoDsd(transaction, backgroundCancellationToken, onlyLockTransaction: true);
            }

            //Function to execute to notify/log that the request is still in progress.
            async Task notifyInProgressTask(CancellationToken backgroundCancellationToken) => await backgroundCancellationToken.SetStillProcessingLogging(transferParam);

            //Action to execute when the application is shutting down
            async void callBackAction() => await unitOfWork.TransactionRepository.MarkTransactionAsCanceled(transferParam.Id);

            _backgroundQueue.Enqueue(new TransactionQueueItem(canBeProcessed, mainTask, notifyInProgressTask, callBackAction));

            return new OperationResult(true, message);
        }

        /// <summary>
        /// Delete all Data DB entries of orphan DSDs and its linked MSDs in the data database for a given dataspace
        /// </summary>
        /// <param name="dataspace"> The ID of the dataspace where the orphan DSDs should be removed. <br></br> Example: <code>design</code>
        /// </param>
        /// <remarks>
        /// Example: Delete all orphan DSDs and its linked MSDs in the dataspace 'design'
        /// 
        ///     DELETE /cleanup/orphans
        ///     {
        ///        "dataspace": "design"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction for a DSD</response>
        /// <response code="207">Multi-Status, containing http status codes 200 and 409</response> 
        [HttpDelete]
        [MapToApiVersion("1.2")]
        [Route("cleanup/orphans")]
        public async Task<ActionResult<OperationResult>> CleanUpOrphanDsds([FromForm, Required] string dataspace)
        {
            var cancellationToken = CancellationToken.None;
            var dataSpace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode);

            var dotStatDbService = _dotStatDbServiceResolver(dataspace);
            var unitOfWork = _unitOfWorkResolver(dataspace);

            var transactionId = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            var transferParam = new TransferParam()
            {
                Id = transactionId,
                TransactionType = TransactionType.CleanupOrphans,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataSpace,
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            IList<ArtefactItem> cleanedDsds = new List<ArtefactItem>();
            IList<ArtefactItem> failedCleanedDsds = new List<ArtefactItem>();

            try
            {
                var dsdsInMgmntDb = await unitOfWork.ArtefactRepository.GetListOfArtefacts(cancellationToken, "DSD");

                foreach (var dsdItm in dsdsInMgmntDb)
                {
                    var dataStructure = dsdItm.ToString().GetDsd(_configuration.DefaultLanguageCode);

                    try
                    {
                        await _commonManager.CleanUpDsd(transferParam, dataStructure, checkExistingDataStructure: true, false, cancellationToken);

                        cleanedDsds.Add(dsdItm);
                    }
                    catch (DatastructureStillExistsException)
                    {
                        //Dsd exists in structure db, no cleaning needed
                    }
                    catch (ConcurrentTransactionException)
                    {
                        failedCleanedDsds.Add(dsdItm);
                    }
                }

                if (failedCleanedDsds.Any() && !cleanedDsds.Any()) //http status code 409 Conflict
                {
                    return new ConflictObjectResult(new OperationResult(false,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpErrorConcurrentTransaction),
                            string.Join(", ", failedCleanedDsds.Select(s => s.ToString())))));
                }

                if (failedCleanedDsds.Any() && cleanedDsds.Any()) //http status code 207 Multi-Status: 409 and 200  
                {
                    return StatusCode(StatusCodes.Status207MultiStatus, new ObjectResult[]
                        {
                            //operation result for http status code 200
                            new OkObjectResult(new OperationResult(true, string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                                "",
                                string.Join(", ", cleanedDsds.Select(s => s.ToString()))))),
                            //operation result for http status code 409
                            new ConflictObjectResult(new OperationResult(false,string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorConcurrentTransaction),
                                string.Join(", ", failedCleanedDsds.Select(s => s.ToString())))))
                        });
                }

                if (!cleanedDsds.Any()) //http status code 200, no orphans to clean
                {
                    return new OperationResult(true,
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.NoOrphanDsdsToClean));
                }

                //200 orphans deleted
                return new OperationResult(
                    true,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                        "",
                        string.Join(", ", cleanedDsds.Select(s => s.ToString()))));
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                Log.Fatal(exception);
                throw;
            }
        }

        /// <summary>
        /// Delete a given DSD and all its child artefacts or only its linked MSD, from the data database, and for a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where the DSD should be cleaned. <br></br> Example: <code>design</code></param>
        /// <param name="dsd">The SDMX ID of the DSD to be deleted <br></br> Format: 'AGENCYID:DSD ID(VERSION)' <br></br> Example: <code>OECD:MEI(1.0)</code></param>
        /// <param name="cleanupMsdOnly">Optional - Indicate if <b>only</b> the linked MSD should be cleaned. By default, it is set to false.</param>
        /// <remarks>
        /// Example: Delete the DSD 'OECD:MEI(1.0)' and all its child artefacts from the 'design' dataspace
        /// 
        ///     DELETE /cleanup/dsd
        ///     {
        ///        "dataspace": "design",
        ///        "dsd": "OECD:MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD</response>////// 
        [HttpDelete]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Route("cleanup/dsd")]
        public async Task<ActionResult<OperationResult>> CleanUpDsd(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dsd,
            [FromForm] bool? cleanupMsdOnly = false)
        {
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                TransactionType = TransactionType.CleanupDsd,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                var dataStructure = dsd.GetDsd(_configuration.DefaultLanguageCode);

                if (!await _commonManager.CleanUpDsd(transferParam, dataStructure, checkExistingDataStructure: false, cleanupMsdOnly ?? false, cancellationToken))
                {
                    // Returns false when there was nothing to delete
                    return new OperationResult(
                        false,
                        string.Format(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorNothingToDelete), dsd));
                }

                var message = string.Format(
                    LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpSuccessful),
                    cleanupMsdOnly ?? false ? DbTypes.GetDbType(SDMXArtefactType.Msd) : string.Empty,
                    dsd);
                Log.Notice(message);

                // Cleanup is successful.
                return new OperationResult(true, message) { Detail = transferParam.Id };
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (ConcurrentTransactionException)
            {
                return new ConflictObjectResult(new OperationResult(false,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .CleanUpErrorConcurrentTransaction), dsd)));
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                Log.Fatal(exception);

                return new OperationResult(false, LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanUpErrorV2)) { Detail = transferParam.Id };
            }
        }

        /// <summary>
        /// Delete all mappingsets and datasets stored in the Mapping Store DB for a given dataflow and a given dataspace
        /// </summary>
        /// <param name="dataspace">The ID of the dataspace where the mappingsets should be cleaned. <br></br> Example: <code>design</code></param>
        /// <param name="dataflow">The SDMX ID of the dataflow for which the mappingsets should be deleted <br></br> Format: 'AGENCYID:DATAFLOWID(VERSION)' <br></br> Example: <code>OECD:DF_MEI(1.0)</code></param>
        /// <remarks>
        /// Example: Delete the mappingsets of the dataflow 'OECD:DF_MEI(1.0)' in the 'design' dataspace
        /// 
        ///     DELETE /cleanup/mappingsets
        ///     {
        ///        "dataspace": "design",
        ///        "dataflow": "OECD:DF_MEI(1.0)"
        ///     }
        ///     
        /// </remarks>
        /// <response code="200">Cleanup succeeded</response>
        /// <response code="400">Data database version is incompatible, Validation or Internal error</response>
        /// <response code="409">Conflict. There is an ongoing transaction targeting the DSD</response>////// 
        [HttpDelete]
        [MapToApiVersion("1.2")]
        [MapToApiVersion("2")]
        [MapToApiVersion("3")]
        [Route("cleanup/mappingsets")]
        public async Task<ActionResult<OperationResult>> CleanUpMappingsets(
            [FromForm, Required] string dataspace,
            [FromForm, Required] string dataflow)
        {
            var cancellationToken = CancellationToken.None;
            var transferParam = new TransferParam()
            {
                TransactionType = TransactionType.CleanupMappingSets,
                CultureInfo = CultureInfo.GetCultureInfo(_configuration.DefaultLanguageCode),
                DestinationDataspace = dataspace.GetSpaceInternal(_configuration, _configuration.DefaultLanguageCode),
                Principal = new DotStatPrincipal(_contextAccessor.HttpContext.User, _authConfiguration.ClaimsMapping),
                TargetVersion = TargetVersion.Live,
                PITReleaseDate = null
            };

            var unitOfWork = _unitOfWorkResolver(transferParam.DestinationDataspace.Id);

            transferParam.Id = await unitOfWork.TransactionRepository.GetNextTransactionId(cancellationToken);

            Log.SetTransactionId(transferParam.Id);
            Log.SetDataspaceId(transferParam.DestinationDataspace.Id);

            try
            {
                var targetDataFlow = dataflow.GetDataflow(_configuration.DefaultLanguageCode);

                if (!await _commonManager.DeleteMappingSets(transferParam, targetDataFlow, cancellationToken))
                {
                    // Returns false when there was nothing to delete
                    return new OperationResult(false,
                        string.Format(
                            LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                                .CleanUpNoMappingSet), targetDataFlow.FullId()));
                }

                // delete is successful.
                return new OperationResult(true,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId.CleanupMappingSetSuccess),
                        targetDataFlow.FullId()));
            }
            catch (TransferUnauthorizedException)
            {
                return new ForbidResult();
            }
            catch (ConcurrentTransactionException)
            {
                return new ConflictObjectResult(new OperationResult(false,
                    string.Format(
                        LocalizationRepository.GetLocalisedResource(Localization.ResourceId
                            .CleanUpErrorMappingSetConcurrentTransaction), dataflow)));
            }
            catch (DotStatException exception)
            {
                Log.Error(exception);
                throw;
            }
            catch (Exception exception)
            {
                Log.Error(string.Format(GetLocalisedResource(Localization.ResourceId.UnhandledExceptionOccurred, transferParam.CultureInfo.TwoLetterISOLanguageName), transferParam.Id));
                Log.Fatal(exception);
                throw;
            }
        }
    }
}
