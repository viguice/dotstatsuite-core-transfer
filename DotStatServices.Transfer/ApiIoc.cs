﻿using DotStat.Common.Auth;
using DotStat.Common.Configuration;
using DotStat.Common.Configuration.Dto;
using DotStat.Common.Configuration.Interfaces;
using DotStat.Common.Localization;
using DotStat.Common.Logger;
using DotStat.Common.Messaging;
using DotStat.Db.DB;
using DotStat.Db.Repository;
using DotStat.Db.Repository.SqlServer;
using DotStat.Db.Service;
using DotStat.Db.Validation;
using DotStat.Db.Validation.SqlServer;
using DotStat.DB;
using DotStat.DB.Repository;
using DotStat.Domain.Cache;
using DotStat.MappingStore;
using DotStat.Transfer;
using DotStat.Transfer.Consumer;
using DotStat.Transfer.Interface;
using DotStat.Transfer.Manager;
using DotStat.Transfer.Messaging.Consumer;
using DotStat.Transfer.Param;
using DotStat.Transfer.Processor;
using DotStat.Transfer.Producer;
using DotStatServices.Transfer.BackgroundJob;
using DotStatServices.Transfer.Services;
using DryIoc;
using Estat.Sri.Mapping.Api.Engine;
using Estat.Sri.Mapping.Api.Factory;
using Estat.Sri.Mapping.Api.Manager;
using Estat.Sri.Mapping.Api.Model;
using Estat.Sri.Mapping.Api.Utils;
using Estat.Sri.Mapping.MappingStore.Manager;
using Estat.Sri.MappingStoreRetrieval.Config;
using Estat.Sri.MappingStoreRetrieval.Factory;
using Estat.Sri.MappingStoreRetrieval.Helper;
using Estat.Sri.Plugin.SqlServer.Engine;
using Estat.Sri.Plugin.SqlServer.Factory;
using Microsoft.AspNetCore.Http.Features;
using Microsoft.Extensions.Configuration;
using Org.Sdmxsource.Sdmx.Api.Manager.Parse;
using Org.Sdmxsource.Sdmx.Structureparser.Manager.Parsing;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace DotStatServices.Transfer
{
    [ExcludeFromCodeCoverage]
    internal class ApiIoc
    {
        public ApiIoc(IContainer container, IConfiguration configuration)
        {
            RegisterServices(container, configuration);
        }

        private static void RegisterServices(IContainer container, IConfiguration configuration)
        {
            var authConfig = configuration.GetSection("auth").Get<AuthConfiguration>() ?? AuthConfiguration.Default;
            var baseConfig = configuration.Get<BaseConfiguration>();
            var formOptions = configuration.Get<FormOptions>();
            var messagingConfig = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            container.RegisterInstance(baseConfig);
            container.RegisterInstance<IDataspaceConfiguration>(baseConfig);
            container.RegisterInstance<ILocalizationConfiguration>(baseConfig);
            container.RegisterInstance<IMailConfiguration>(baseConfig);
            container.RegisterInstance<IGeneralConfiguration>(baseConfig);
            container.RegisterInstance<IAuthConfiguration>(authConfig);
            LocalizationRepository.Configure(baseConfig);
            container.RegisterInstance(formOptions);


            // NSI configuration ----------------------
            ConfigManager.Config.DataflowConfiguration.IgnoreProductionForStructure = true;
            ConfigManager.Config.DataflowConfiguration.IgnoreProductionForData = true;
            

            // DI registration ------------------------

            // Log4Net
            LogHelper.ConfigureAppenders(baseConfig);
            
            MappingStoreIoc.Container.Register<IRetrievalEngineContainerFactory, RetrievalEngineContainerFactory>(Reuse.Singleton);
            container.Register<IObservationRepository, SqlObservationRepository>(Reuse.Singleton);
            container.Register<IAttributeRepository, SqlAttributeRepository>(Reuse.Singleton);

            container.Register<ITransferProcessor, NoneTransferProcessor>(Reuse.Singleton);

            container.Register<ITransferManager<ExcelToSqlTransferParam>, ExcelToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<SqlToSqlTransferParam>, SqlToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<SdmxFileToSqlTransferParam>, SdmxFileToSqlTransferManager>(Reuse.Transient);
            container.Register<ITransferManager<SdmxUrlToSqlTransferParam>, SdmxUrlToSqlTransferManager>(Reuse.Transient);
            container.Register<CommonManager>(Reuse.Transient);

            container.Register<IProducer<ExcelToSqlTransferParam>, ExcelProducer>(Reuse.Transient);
            container.Register<IProducer<SqlToSqlTransferParam>, SqlProducer<SqlToSqlTransferParam>>(Reuse.Transient);
            container.Register<IProducer<SdmxFileToSqlTransferParam>, SdmxFileProducer>(Reuse.Transient);
            container.Register<IProducer<SdmxUrlToSqlTransferParam>, SdmxUrlProducer>(Reuse.Transient);
            
            container.Register<IConsumer<ExcelToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(ExcelToSqlTransferManager)));

            container.Register<IConsumer<SqlToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(SqlToSqlTransferManager)));

            container.Register<IConsumer<SdmxFileToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(SdmxFileToSqlTransferManager)));

            container.Register<IConsumer<SdmxUrlToSqlTransferParam>, SqlConsumer>(
                Reuse.Transient,
                setup: Setup.With(condition: r => r.Parent.ImplementationType == typeof(SdmxUrlToSqlTransferManager)));

            //Mapping store 
            var assemblies = new[]
            {
                typeof(DatabaseManager).Assembly,
                typeof(IEntityRetrieverManager).Assembly
            };

            container.RegisterMany(assemblies,
                type =>
                    !typeof(IEntity).IsAssignableFrom(type) && !typeof(Exception).IsAssignableFrom(type) && type != typeof(SingleRequestScope), reuse: Reuse.Singleton, made: FactoryMethod.ConstructorWithResolvableArguments);

            container.Register<IDatabaseProviderEngine, SqlServerDatabaseProviderEngine>();
            container.Register<IDatabaseProviderFactory, SqlServerDatabaseProviderFactory>();
            
            container.Register<ICache, MemoryCache>(Reuse.Scoped);
            container.Register<IMappingStoreDataAccess, MappingStoreDataAccess>(Reuse.Scoped);

            container.Register<IMailService, MailService>(Reuse.Singleton);

            if (authConfig.Enabled)
            {
                container.Register<IAuthorizationRepository, SqlAuthorizationRepository>(Reuse.Singleton);
                container.Register<IAuthorizationManagement, AuthorizationManagement>(Reuse.Singleton);
            }
            else
            {
                container.Register<IAuthorizationManagement, NoAuthorizationManagement>(Reuse.Singleton);
            }

            container.Register<ITempFileManager, TempFileManager>(Reuse.Singleton);
            container.RegisterMapping<ITempFileManagerBase, ITempFileManager>();

            container.Register<IConsumerFactory, ConsumerFactory>(Reuse.Singleton);

            //Repositories
            container.RegisterInstance(SupportedDatabaseVersion.DataDbVersion);

            foreach (var dataSpace in baseConfig.SpacesInternal)
            {
                container.RegisterInstance(dataSpace, serviceKey: dataSpace.Id);
                container.Register<IDotStatDb, SqlDotStatDb>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<DataspaceInternal>(serviceKey: dataSpace.Id));

                container.Register<SqlDotStatDb>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<DataspaceInternal>(serviceKey: dataSpace.Id));

                container.Register<IArtefactRepository, SqlArtefactRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IAttributeRepository, SqlAttributeRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IDataStoreRepository, SqlDataStoreRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IMetadataComponentRepository, SqlMetadataComponentRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IMetadataStoreRepository, SqlMetadataStoreRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IObservationRepository, SqlObservationRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<ITransactionRepository, SqlTransactionRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IComponentRepository, SqlComponentRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<ICodelistRepository, SqlCodelistRepository>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<SqlDotStatDb>(serviceKey: dataSpace.Id));

                container.Register<IUnitOfWork, UnitOfWork>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of
                    .Type<IDotStatDb>(serviceKey: dataSpace.Id)
                    .Type<IArtefactRepository>(serviceKey: dataSpace.Id)
                    .Type<IAttributeRepository>(serviceKey: dataSpace.Id)
                    .Type<IDataStoreRepository>(serviceKey: dataSpace.Id)
                    .Type<IMetadataComponentRepository>(serviceKey: dataSpace.Id)
                    .Type<IMetadataStoreRepository>(serviceKey: dataSpace.Id)
                    .Type<IObservationRepository>(serviceKey: dataSpace.Id)
                    .Type<ITransactionRepository>(serviceKey: dataSpace.Id)
                    .Type<IComponentRepository>(serviceKey: dataSpace.Id)
                    .Type<ICodelistRepository>(serviceKey: dataSpace.Id));

                container.Register<IDotStatDbService, DotStatDbService>(
                    serviceKey: dataSpace.Id, reuse: Reuse.Singleton,
                    made: Parameters.Of.Type<IUnitOfWork>(serviceKey: dataSpace.Id));

            }

            container.RegisterDelegate<DotStatDbResolver>(r =>
                serviceKey => r.Resolve<IDotStatDb>(serviceKey), reuse: Reuse.Singleton
            );

            container.RegisterDelegate<UnitOfWorkResolver>(r =>
                serviceKey => r.Resolve<IUnitOfWork>(serviceKey), reuse: Reuse.Singleton
            );

            container.RegisterDelegate<DotStatDbServiceResolver>(r =>
                serviceKey => r.Resolve<IDotStatDbService>(serviceKey), reuse: Reuse.Singleton
            );

            //Validators
            container.Register<IDatasetAttributeValidator, DatasetAttributeValidator>(reuse: Reuse.Scoped);

            //Database specific, validators
            container.Register<IDatasetAttributeDatabaseValidator, SqlDatasetAttributeDatabaseValidator>(reuse: Reuse.Scoped);
            container.Register<IKeyableDatabaseValidator, SqlKeyableDatabaseValidator>(reuse: Reuse.Scoped);
            
            MappingStoreIoc.Container.RegisterMany(
                assemblies,
                type => !typeof(IEntity).IsAssignableFrom(type),
                reuse: Reuse.Singleton,
                made: FactoryMethod.ConstructorWithResolvableArguments,
                setup: Setup.With(allowDisposableTransient: true),
                ifAlreadyRegistered: IfAlreadyRegistered.Keep);

            MappingStoreIoc.Container.Register<IStructureParsingManager, StructureParsingManager>(Reuse.Singleton, ifAlreadyRegistered: IfAlreadyRegistered.Keep, made: FactoryMethod.ConstructorWithResolvableArguments);

            var mappingStoreConnectionManager = new MappingStoreConnectionManager(baseConfig);
            MappingStoreIoc.Container.RegisterInstance<IConfigurationStoreManager>(mappingStoreConnectionManager, IfAlreadyRegistered.Replace);
            container.RegisterInstance<IConfigurationStoreManager>(mappingStoreConnectionManager, IfAlreadyRegistered.Replace);
        }
    }
    internal sealed class MappingStoreConnectionManager : Estat.Sri.Mapping.Api.Manager.IConfigurationStoreManager
    {
        private readonly IList<ConnectionStringSettings> _connections;

        public MappingStoreConnectionManager(IDataspaceConfiguration config)
        {
            _connections = config.SpacesInternal.Select(space => new ConnectionStringSettings(
                $"DotStatSuiteCoreStructDb_{space.Id}",
                space.DotStatSuiteCoreStructDbConnectionString,
                "System.Data.SqlClient"
             )).ToArray();
        }

        public IEnumerable<TSettings> GetSettings<TSettings>()
        {
            return typeof(TSettings) == typeof(ConnectionStringSettings)
                ? (IEnumerable<TSettings>)_connections
                : null;
        }

        public IActionResult SaveSettings<TSettings>(TSettings settings) => throw new NotImplementedException();
        public IActionResult DeleteSettings<TTettings>(string name) => throw new NotImplementedException();
    }
}