﻿using DotStat.Transfer;
using DotStat.Transfer.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc.ModelBinding;
using System.IO;
using System.Threading.Tasks;

namespace DotStatServices.Transfer.BackgroundJob
{
    public interface ITempFileManager : ITempFileManagerBase
    {
        public Task<FormValueProvider> StreamBodyToDisk(HttpRequest request, IFileParameter parameters);

        public FileStream TryCreateTempFileStream(string filename = null);

        public bool DeleteTempFile(string file);

        public void ClearTempFiles();
    }
}
