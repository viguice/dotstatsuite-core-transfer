﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Text;
using System.Threading;
using DotStat.Common.Messaging;
using Estat.Sdmxsource.Extension.Model.Error;
using log4net;
using Microsoft.Extensions.Configuration;
using Newtonsoft.Json;
using Org.Sdmxsource.Sdmx.Api.Constants;
using Org.Sdmxsource.Sdmx.Util.Objects.Reference;
using RabbitMQ.Client;
using RabbitMQ.Client.Exceptions;

namespace DotStat.Transfer.Messaging.Producer
{
    public class RabbitMqProducer : IProducer, IDisposable
    {
        private readonly MessagingServiceConfiguration _msgBrokerConf;

        //Create global connection since its recommended to keep connection and channel open, https://www.rabbitmq.com/dotnet-api-guide.html#connection-and-channel-lifspan
        //Abstract IConnection to be generic, not RabbitMQ dependent 
        private IConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _channel;
        private static readonly ILog _logger = LogManager.GetLogger(typeof(RabbitMqProducer));
        private static string _dataspace;


        public RabbitMqProducer(IConfiguration configuration, IConnectionFactory connectionFactory)
        {
            _logger.Info("Starting Messaging service");

            _msgBrokerConf = configuration.GetSection(MessagingServiceConfiguration.MessageBrokerSettings).Get<MessagingServiceConfiguration>();
            _dataspace = configuration.GetSection("mappingStore:Id").GetValue<string>("Default");

            _connectionFactory = connectionFactory ??= new ConnectionFactory
            {
                HostName = _msgBrokerConf.HostName,
                VirtualHost = _msgBrokerConf.VirtualHost,
                Port = _msgBrokerConf.Port,
                UserName = _msgBrokerConf.UserName,
                Password = _msgBrokerConf.Password
            };

            if (_msgBrokerConf?.Enabled == true)
            {
                CreateConnection();
            }
        }




        /// <summary>
        /// 
        /// </summary>
        /// <param name="summary"></param>
        /// <param name="principal"></param>
        /// <returns></returns>
        public void Notify(IList<IResponseWithStatusObject> summary, IPrincipal principal)
        {
            if (_msgBrokerConf?.Enabled == true)
            {
                if (_connection == null || _channel == null)
                {
                    CreateConnection();
                }

                if (summary?.Count > 0)
                {
                    var props = _channel!.CreateBasicProperties();

                    props.DeliveryMode =
                        _msgBrokerConf.DeliveryMode; //Set delivery mode to 2 for persisting queue items

                    var queueItem = new QueueItem(
                        _dataspace,
                        (principal.Identity as ClaimsIdentity)?.FindFirst("name")?.Value,
                        (principal.Identity as ClaimsIdentity)?.FindFirst("email")?.Value,
                        summary
                            .Where(y => y.StructureReference.MaintainableStructureEnumType ==
                                        SdmxStructureEnumType.Dataflow && y.Action.ToDatasetActionEnumType() == DatasetActionEnumType.Append)
                            .Select(x => x.StructureReference.MaintainableReference).Cast<MaintainableRefObjectImpl>());

                    var body = Encoding.UTF8.GetBytes(
                        JsonConvert.SerializeObject(queueItem)
                    );

                    try
                    {
                        _channel.BasicPublish(
                            exchange: "", //use default exchange
                            routingKey: _msgBrokerConf.QueueName,
                            basicProperties: props,
                            body: body);
                    }
                    catch (System.Exception e)
                    {
                        _logger.Error("Failed to insert item into queue " + _msgBrokerConf.QueueName, e);
                        Thread.Sleep(TimeSpan.FromMinutes(3));
                    }
                }
            }

        }

        public void Dispose()
        {
            if (_connection?.IsOpen == true)
                _connection.Close(); //Closes connection and all its channels
        }

        /// <summary>
        /// Creates connection to the messagebroker
        /// </summary>
        private void CreateConnection()
        {
            //Try to connect 5 times if rabbitmq is in starting up phase
            for (int i = 0; i <= 5; i++)
            {
                try
                {
                    _connection = _connectionFactory.CreateConnection();
                    _channel = _connection.CreateModel();
                    _channel.QueueDeclare(
                        queue: _msgBrokerConf.QueueName,
                        durable: _msgBrokerConf.QueueDurable,
                        exclusive: false,
                        autoDelete: false,
                        arguments: null);

                    _connection.ConnectionShutdown += (sender, ea) =>
                    {
                        //if messagebroker is unreachable, try to restart 
                        CreateConnection();
                    };

                    break;
                }
                catch (System.Exception e) when (e is BrokerUnreachableException || e is OperationInterruptedException)
                {
                    if (i < 5)
                        Thread.Sleep(TimeSpan.FromSeconds(10));
                    else
                        throw e;
                }
            }
        }

    }
}
