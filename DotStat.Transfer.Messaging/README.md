
# README #
This project uses the Plugin feature in the NSIWS to synchronize the MappingStore/Structure database with the DotStat data database.

## Setup ##

1. Rabbit MQ software setup  
> Either use Docker by running command `docker run -it --rm --name rabbitmq -p 5672:5672 -p 15672:15672 rabbitmq:3-management`   
> or by follow installtion guide on https://www.rabbitmq.com/download.html     
> Once the installtion is completed, verify that the RabbitMq service is up and running by navigating to http://localhost:15672/, the username and password are guest/guest
> In this example the default configuration settings will be used
     
2. Dotstat plugin setup 
   i. Build and publish project by running command **dotnet publish -o out**
   ii. Copy all files in **out** folder to the **Plugin** folder located in the NSIWS root folder 
   iii. Copy **RabbitMq.json.sample** to the NSIWS config folder and remove **sample** suffix from the filename or use other methods for setting variables, such as environment variables. 
   iiii. Verify that the plugin is working by uploading a DSD using the NSIWS rest interface, this should create all database objects in the Dotstat data database.

3. Persist to not lose any data setting the queue the queue to persist and in case of using Docker, then map volumes as example below.

    rabbit-mq:
        image: rabbitmq:3.8.9-management
        container_name: rabbit-mq
        hostname: my-rabbit-mq
        ports:
          - "15672:15672"
          - "5672:5672"
        volumes:
          - "./rabbitmq/data/:/var/lib/rabbitmq/mnesia/"    #data 
          - "./logs:/var/log/rabbitmq/log"     #logs     
        networks:
          - my_network
    
   




