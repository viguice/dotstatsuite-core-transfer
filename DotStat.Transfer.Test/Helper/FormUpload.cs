﻿using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Text;
using Microsoft.AspNetCore.Http;

namespace DotStat.Transfer.Test.Helper
{
    [ExcludeFromCodeCoverage]
    public static class FormUpload
    {
        private static readonly Encoding Encoding = Encoding.UTF8;

        [ExcludeFromCodeCoverage]
        public static byte[] GetMultipartFormDataAsync( Dictionary<string, object> postParameters, string boundary)
        {
            Stream formDataStream = new MemoryStream();
            var needsClrf = false;

            foreach (var (key, value) in postParameters)
            {
                // Skip it on the first parameter, add it to subsequent parameters.
                if (needsClrf)
                    formDataStream.Write(Encoding.GetBytes("\r\n"), 0, Encoding.GetByteCount("\r\n"));

                needsClrf = true;

                if (value is FileParameter fileToUpload)
                {
                    // Add just the first part of this param, since we will write the file data directly to the Stream
                    var header =
                        $"--{boundary}\r\nContent-Disposition: form-data; name=\"{key}\"; filename=\"{fileToUpload.FileName ?? key}\"\r\nContent-Type: {fileToUpload.ContentType ?? "application/octet-stream"}\r\n\r\n";

                    formDataStream.Write(Encoding.GetBytes(header), 0, Encoding.GetByteCount(header));

                    // Write the file data directly to the Stream, rather than serializing it to a string.
                    formDataStream.Write(fileToUpload.File, 0, fileToUpload.File.Length);
                }
                else
                {
                    var postData = $"--{boundary}\r\nContent-Disposition: form-data; name=\"{key}\"\r\n\r\n{value}";
                    formDataStream.Write(Encoding.GetBytes(postData), 0, Encoding.GetByteCount(postData));
                }
            }

            // Add the end of the request.  Start with a newline
            var footer = "\r\n--" + boundary + "--\r\n";
            formDataStream.Write(Encoding.GetBytes(footer), 0, Encoding.GetByteCount(footer));

            // Dump the Stream into a byte[]
            formDataStream.Position = 0;
            var formData = new byte[formDataStream.Length];
            formDataStream.Read(formData, 0, formData.Length);
            formDataStream.Close();

            return formData;
        }

        [ExcludeFromCodeCoverage]
        public static IFormFile GetFile(string name, string filename, string contentType = "")
        {
            var fi = new FileInfo(filename);
            var formFile = new FormFile(fi.OpenRead(), 0, fi.Length, name, fi.Name)
            {
                Headers = new HeaderDictionary { { "Content-Type", contentType } }
            };
            return formFile;
        }

        [ExcludeFromCodeCoverage]
        public class FileParameter
        {
            public byte[] File { get; set; }
            public string FileName { get; set; }
            public string ContentType { get; set; }
            public FileParameter(byte[] file) : this(file, null) { }
            public FileParameter(byte[] file, string filename) : this(file, filename, null) { }
            public FileParameter(byte[] file, string filename, string contentType)
            {
                File = file;
                FileName = filename;
                ContentType = contentType;
            }
        }
    }
}
