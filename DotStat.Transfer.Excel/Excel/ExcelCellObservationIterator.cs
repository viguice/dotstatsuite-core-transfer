﻿using System.Linq;
using DotStat.Common.Localization;
using DotStat.Transfer.Excel.Exceptions;
using DotStat.Transfer.Excel.Mapping;
using DotStat.Transfer.Excel.Reader;
using DotStat.Transfer.Excel.Util;
using Org.Sdmxsource.Sdmx.Api.Model.Data;
using Attribute = DotStat.Domain.Attribute;
using Dimension = DotStat.Domain.Dimension;

namespace DotStat.Transfer.Excel.Excel
{
    public sealed class ExcelCellObservationIterator : ExcelCellRangeIterator, IRecordIterator<IObservation>
    {
        private string _CurrentPrimaryValue;
        private CellReference _CurrentPrimaryValueCell;

        private new ExcelCoordMappingDescriptor CoordDescriptor { get;}

        public ExcelCellObservationIterator(ExcelCoordMappingDescriptor coordDescriptor, IExcelDataSource dataSource, string sheetName) : base(coordDescriptor, dataSource, sheetName)
        {
            CoordDescriptor = coordDescriptor;

            BuildFieldList();
            InitExpressions();
            Reset();
        }

        V8Mapper<IObservation> IRecordIterator<IObservation>.Mapper => CoordDescriptor.Mapper;

        public override SWMapper Mapper => CoordDescriptor.Mapper;

        public override string Name => CoordDescriptor.Name;


        /// <summary>
        /// code/index maping: value, dimensions, observation attributes
        /// </summary>
        private void BuildFieldList()
        {
            var pos = 0;

            _FieldsByName.Add(CoordDescriptor.Mapper.ValueFieldName, pos);
            _FieldsById.Add(pos++, CoordDescriptor.Mapper.ValueFieldName);

            foreach (var dim in CoordDescriptor.Mapper.TargetDataset.Dimensions)
            {
                _FieldsByName.Add(dim.FullCode, pos);
                _FieldsById.Add(pos++, dim.FullCode);
            }

            foreach (var attr in CoordDescriptor.Mapper.TargetDataset.ObsAttributes)
            {
                _FieldsByName.Add(attr.FullCode, pos);
                _FieldsById.Add(pos++, attr.FullCode);
            }
        }

        private void InitExpressions()
        {
            int index;

            foreach (var dimExpr in CoordDescriptor.DimensionExpressions)
            {
                if (!_FieldsByName.TryGetValue(Dimension.GetFullCode(dimExpr.Code), out index))
                    throw new ApplicationArgumentException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.DimensionDoesNotBelongToDataset).F(dimExpr.Code, CoordDescriptor.Mapper.TargetDataset.Code));

                dimExpr.Init(index, this);
            }

            foreach (var attrExpr in CoordDescriptor.ObsAttributeExpressions)
            {
                if (!_FieldsByName.TryGetValue(Attribute.GetFullCode(attrExpr.Code), out index))
                    throw new ApplicationArgumentException(LocalizationRepository.GetLocalisedResource(Localization.ResourceId.AttributeDoesNotBelongToDataset).F(attrExpr.Code, CoordDescriptor.Mapper.TargetDataset.Code));

                attrExpr.Init(index, this);
            }
        }

        public override string GetField(int pos)
        {
            if (pos == 0)
                return GetValue();

            if (pos < ExternCoordinates.Length)
                return ExternCoordinates[pos].Value;

            //data source name
            return _Current.FullCoordinates;
        }

        protected override CellValue[] GetExternCoordinates()
        {
            var res = new CellValue[FieldCount];

            foreach (var dimExpr in CoordDescriptor.DimensionExpressions)
            {
                res[dimExpr.Index].Value = dimExpr.Result;
                res[dimExpr.Index].Source = "Excel: " + dimExpr.EvaluationSource + ", EDD: " + dimExpr.ExpressionSource;
            }

            foreach (var attrExpr in CoordDescriptor.ObsAttributeExpressions)
            {
                res[attrExpr.Index].Value = attrExpr.Result;
                res[attrExpr.Index].Source = "Excel: " + attrExpr.EvaluationSource + ", EDD: " + attrExpr.ExpressionSource;
            }

            return res;
        }

        protected override bool IsCellExcluded()
        {
            return CoordDescriptor.ExcludeCellConditions.Any(cc => cc.BooleanResult);
        }

        /// <exception cref="DataFormatException">If the cell does not contain a valid value (like DIV0#) and no Data-Cell element is made.</exception>
        private string GetValue()
        {
            if (CoordDescriptor.PrimaryValueExpression != null)
            {
                return CurrentPrimaryValue;
            }
            var cell = CurrentPrimaryValueCell;
            if (!DataSource.IsCellValueValid(cell.Row, cell.Column, cell.SheetName))
            {
                throw new DataFormatException(string.Format(
                    LocalizationRepository.GetLocalisedResource(
                        Localization.ResourceId.ExcelInvalidDataValue),
                    _Current.FullCoordinates, CoordDescriptor.Mapper.ValueFieldName));
            }
            return DataSource.GetFormattedCellValue(_Current.Row, _Current.Column, SheetName);
        }

        private CellReference CurrentPrimaryValueCell
        {
            get
            {
                if (_CurrentPrimaryValueCell != null)
                {
                    return _CurrentPrimaryValueCell;
                }
                if (CoordDescriptor.PrimaryValueExpression != null)
                {
                    CoordDescriptor.PrimaryValueExpression.Owner = this;
                    _CurrentPrimaryValueCell = CoordDescriptor.PrimaryValueExpression.CellReferenceResult;
                }
                if (_CurrentPrimaryValueCell == null)
                {
                    _CurrentPrimaryValueCell = _Current;
                }
                return _CurrentPrimaryValueCell;
            }
        }

        private string CurrentPrimaryValue
        {
            get
            {
                if (_CurrentPrimaryValue != null)
                {
                    return _CurrentPrimaryValue;
                }
                if (CoordDescriptor.PrimaryValueExpression != null)
                {
                    CoordDescriptor.PrimaryValueExpression.Owner = this;
                    _CurrentPrimaryValue = CoordDescriptor.PrimaryValueExpression.Result;
                }
                else
                {
                    _CurrentPrimaryValue = CurrentPrimaryValueCell.Value;
                }
                return _CurrentPrimaryValue;
            }
        }

        protected override void ResetCurrentDataExpressions(bool resetOwner = false)
        {
            base.ResetCurrentDataExpressions(resetOwner);

            TargetRecord = null;
            _CurrentPrimaryValueCell = null;
            _CurrentPrimaryValue = null;

            foreach (var dc in CoordDescriptor.DimensionExpressions)
            {
                if (resetOwner || dc.Owner != this)
                {
                    dc.Owner = this;
                }
                else
                {
                    dc.Reset();
                }
            }

            foreach (var dc in CoordDescriptor.ObsAttributeExpressions)
            {
                if (resetOwner || dc.Owner != this)
                {
                    dc.Owner = this;
                }
                else
                {
                    dc.Reset();
                }
            }

            if (CoordDescriptor.PrimaryValueExpression != null)
            {
                if (resetOwner || CoordDescriptor.PrimaryValueExpression.Owner != this)
                {
                    CoordDescriptor.PrimaryValueExpression.Owner = this;
                }
                else
                {
                    CoordDescriptor.PrimaryValueExpression.Reset();
                }
            }
        }

        public override void Reset()
        {
            base.Reset();

            _CurrentPrimaryValue = null;
            _CurrentPrimaryValueCell = null;
        }

        internal static ExcelCellObservationIterator GetDummyInstance(ExcelCoordMappingDescriptor cmd, string templateName)
        {
            return new ExcelCellObservationIterator(cmd, new DummyExcelDataSource(cmd, templateName), "Dummy");
        }
    }
}
