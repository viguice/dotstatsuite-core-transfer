﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Transfer.Excel.Util
{
    public static class Collections
    {
        public static int MaxOrDefault<T>(this IEnumerable<T> coll, Func<T, int> func, int def = 0)
        {
            var elems = false;
            var max = int.MinValue;
            foreach (var elem in coll)
            {
                elems = true;
                var res = func(elem);
                if (res > max)
                {
                    max = res;
                }
            }
            return elems ? max : def;
        }

        public static void RemoveAll(this CollectionBase cool, Func<object, bool> filter)
        {
            var idx = 0;
            var rl = new LinkedList<int>();
            foreach (var elem in cool)
            {
                if (filter(elem))
                {
                    rl.AddLast(idx);
                }
                idx++;
            }
            var node = rl.Last;
            while (node != null)
            {
                cool.RemoveAt(node.Value);
                node = node.Previous;
            }
        }

        public static IEnumerable<int> GetSequence(this int start, int end, int increment = 1)
        {
            while (start <= end)
            {
                yield return start;
                start += increment;
            }
        }

        public static IIndexer<T> ToIndexer<T>(this IEnumerable<T> coll)
        {
            return new Indexer<T>(coll);
        }

        public static bool SetEquals<T>(this IIndexer<T> target, IIndexer<T> source, IEqualityComparer<T> comparer)
        {
            if (target == null && source == null)
            {
                return true;
            }
            if (source == null || target == null)
            {
                return false;
            }
            if (source.Count != target.Count)
            {
                return false;
            }
            return !source.Where((t, ii) => !comparer.Equals(t, target[ii])).Any();
        }

        public static LinkedList<T> ToLinkedList<T>(this IEnumerable<T> target)
        {
            var res = target as LinkedList<T>;
            if (res != null)
            {
                return res;
            }
            res = new LinkedList<T>();
            foreach (var ce in target)
            {
                res.AddLast(ce);
            }
            return res;
        }

        public static int IndexOf<T>(this IEnumerable<T> target, Predicate<T> predicate)
        {
            var idx = 0;
            foreach (var ce in target)
            {
                if (predicate(ce))
                {
                    return idx;
                }
                idx++;
            }
            return -1;
        }

        public static int IndexOf<T>(this IEnumerable<T> target, T elem, IEqualityComparer<T> comparer)
        {
            var idx = 0;
            foreach (var ce in target)
            {
                if (comparer.Equals(elem, ce))
                {
                    return idx;
                }
                idx++;
            }
            return -1;
        }

        public static void AddAll<T>(this ICollection<T> target, IEnumerable<T> items)
        {
            foreach (T item in items)
            {
                target.Add(item);
            }
        }


        public static IEnumerable<IEnumerable<T>> CartesianProduct<T>(this IEnumerable<IEnumerable<T>> sequences)
        {
            // ReSharper disable PossibleMultipleEnumeration
            IEnumerable<IEnumerable<T>> emptyProduct = new[] {Enumerable.Empty<T>()};
            return sequences.Aggregate(emptyProduct,
                (accumulator, sequence) => from accseq in accumulator
                    from item in sequence
                    select accseq.Concat(new[] {item}));
            // ReSharper restore PossibleMultipleEnumeration
        }

        public static IIndexer<T> Wrap<T>(this IList<T> list)
        {
            return new IndexerWrapper<T>(list);
        }

        public static IIndexer<T> Wrap<T>(this IEnumerable<T> coll)
        {
            var indexer = coll as IIndexer<T>;
            return indexer ?? new IndexerWrapper<T>(new List<T>(coll));
        }


        public static void ForEach<T>(this IEnumerable<T> coll, Action<T> action)
        {
            foreach (T item in coll)
            {
                action(item);
            }
        }

        public static void ForEach<T>(this IList<T> coll, Action<int, T> action)
        {
            for (var ii = 0; ii < coll.Count; ii++)
            {
                action(ii, coll[ii]);
            }
        }

        public static void Sort<T>(this IList coll, Func<T, T, int> compareFunc)
        {
            QuickSort(coll, 0, coll.Count - 1, compareFunc);
        }

        private static void SwapIfGreaterWithItems<T>(IList elements, int a, int b, Func<T, T, int> compareFunc)
        {
            if (a != b && compareFunc((T)elements[a], (T)elements[b]) > 0)
            {
                var obj = elements[a];
                elements[a] = elements[b];
                elements[b] = obj;
            }
        }

        private static void QuickSort<T>(IList elements, int left, int right, Func<T, T, int> compareFunc)
        {
            if (elements.Count < 1)
            {
                return;
            }
            do
            {
                var num = left;
                var num2 = right;
                var median = num + ((num2 - num) >> 1);
                SwapIfGreaterWithItems(elements, num, median, compareFunc);
                SwapIfGreaterWithItems(elements, num, num2, compareFunc);
                SwapIfGreaterWithItems(elements, median, num2, compareFunc);
                var obj = (T)elements[median];
                do
                {
                    while (compareFunc((T)elements[num], obj) < 0)
                    {
                        num++;
                    }
                    while (compareFunc(obj, (T)elements[num2]) < 0)
                    {
                        num2--;
                    }
                    if (num > num2)
                    {
                        break;
                    }
                    if (num < num2)
                    {
                        var obj2 = elements[num];
                        elements[num] = elements[num2];
                        elements[num2] = obj2;
                    }
                    num++;
                    num2--;
                } while (num <= num2);
                if (num2 - left <= right - num)
                {
                    if (left < num2)
                    {
                        QuickSort(elements, left, num2, compareFunc);
                    }
                    left = num;
                }
                else
                {
                    if (num < right)
                    {
                        QuickSort(elements, num, right, compareFunc);
                    }
                    right = num2;
                }
            } while (left < right);
        }

        public class ArrayEqualityComparer<T> : IEqualityComparer<T[]>
        {
            private readonly IEqualityComparer<T> _Comparer;
            public static readonly ArrayEqualityComparer<T> Instance = new ArrayEqualityComparer<T>();

            public ArrayEqualityComparer(IEqualityComparer<T> comparer = null)
            {
                _Comparer = comparer ?? EqualityComparer<T>.Default;
            }


            public bool Equals(T[] x, T[] y)
            {
                if (x == null && y == null)
                {
                    return true;
                }
                if (x == null || y == null)
                {
                    return false;
                }
                if (x.Length != y.Length)
                {
                    return false;
                }
                for (int ii = 0; ii < x.Length; ii++)
                {
                    if (!_Comparer.Equals(x[ii], y[ii]))
                    {
                        return false;
                    }
                }
                return true;
            }

            public int GetHashCode(T[] obj)
            {
                unchecked
                {
                    var res = 7;
                    for (var ii = 0; ii < obj.Length; ii++)
                    {
                        var item = obj[ii];
                        if (!Equals(item, default(T)))
                        {
                            res = res*31 + item.GetHashCode();
                        }
                    }
                    return res;
                }
            }
        }

        private sealed class IndexerWrapper<T> : IIndexer<T>
        {
            private readonly IList<T> _Inner;

            public IndexerWrapper(IList<T> list)
            {
                _Inner = list;
            }

            IEnumerator<T> IEnumerable<T>.GetEnumerator()
            {
                return _Inner.GetEnumerator();
            }

            IEnumerator IEnumerable.GetEnumerator()
            {
                return _Inner.GetEnumerator();
            }

            void ICollection.CopyTo(Array array, int index)
            {
                _Inner.CopyTo((T[])array, index);
            }

            int ICollection.Count
            {
                get { return _Inner.Count; }
            }

            object ICollection.SyncRoot
            {
                get { return ((ICollection)_Inner).SyncRoot; }
            }

            bool ICollection.IsSynchronized
            {
                get { return ((ICollection)_Inner).IsSynchronized; }
            }

            T IIndexer<T>.this[int idx]
            {
                get { return _Inner[idx]; }
            }

            int IIndexer<T>.IndexOf(T elem)
            {
                return _Inner.IndexOf(elem);
            }

            bool IIndexer<T>.Contains(T item)
            {
                return _Inner.Contains(item);
            }
        }
    }
}