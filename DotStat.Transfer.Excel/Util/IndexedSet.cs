﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DotStat.Transfer.Excel.Util
{
    public class IndexedSet<T> : IIndexedSet<T>
    {
        private readonly Dictionary<T, int> _ItemIndex;
        private readonly List<T> _Items;

        private readonly IEqualityComparer<T> _Comparer;

        public IndexedSet(IEnumerable<T> items, IEqualityComparer<T> comp = null, int capacity = 10)
            : this(comp, capacity)
        {
            AddRange(items);
        }

        public IndexedSet(IEqualityComparer<T> comp = null, int capacity = 10)
        {
            _Comparer = comp ?? EqualityComparer<T>.Default;
            _ItemIndex = new Dictionary<T, int>(capacity, _Comparer);
            _Items = new List<T>(capacity);
        }

        IEnumerator<T> IEnumerable<T>.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return _Items.GetEnumerator();
        }

        void ICollection.CopyTo(Array array, int index)
        {
            _Items.CopyTo((T[])array, index);
        }

        public bool Remove(T item)
        {
            int idx;
            if (_ItemIndex.TryGetValue(item, out idx))
            {
                _ItemIndex.Remove(item);
                _Items.RemoveAt(idx);
                if (idx != _Items.Count)
                {
                    AdjustMembers();
                }
                return true;
            }
            return false;
        }

        public int Count
        {
            get { return _Items.Count; }
        }

        bool ICollection<T>.IsReadOnly
        {
            get { return false; }
        }

        object ICollection.SyncRoot
        {
            get { return _Items; }
        }

        bool ICollection.IsSynchronized
        {
            get { return false; }
        }

        public T this[int idx]
        {
            get { return _Items[idx]; }
        }

        public int IndexOf(T elem)
        {
            int idx;
            if (_ItemIndex.TryGetValue(elem, out idx))
            {
                return idx;
            }
            return -1;
        }

        public void Add(T item)
        {
            if (!_ItemIndex.ContainsKey(item))
            {
                _Items.Add(item);
                _ItemIndex.Add(item, _Items.Count - 1);
            }
        }

        public void AddOrReplace(T item)
        {
            int idx;
            if (!_ItemIndex.TryGetValue(item, out idx))
            {
                _Items.Add(item);
                _ItemIndex.Add(item, _Items.Count - 1);
            }
            else
            {
                _Items[idx] = item;
            }
        }

        public void Clear()
        {
            _ItemIndex.Clear();
            _Items.Clear();
        }

        public bool Contains(T item)
        {
            return _ItemIndex.ContainsKey(item);
        }

        void ICollection<T>.CopyTo(T[] array, int arrayIndex)
        {
            _Items.CopyTo(array, arrayIndex);
        }

        public void AddRange(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                Add(item);
            }
        }

        public void RemoveAll(IEnumerable<T> items)
        {
            foreach (var item in items)
            {
                int idx;
                if (_ItemIndex.TryGetValue(item, out idx))
                {
                    _ItemIndex.Remove(item);
                }
            }
            if (_Items.Count > _ItemIndex.Count)
            {
                AdjustMembers();
            }
        }

        private void AdjustMembers()
        {
            int ii = 0;
            foreach (var kv in _ItemIndex.OrderBy(it => it.Value))
            {
                _ItemIndex[kv.Key] = ii;
                _Items[ii] = kv.Key;
                ii++;
            }
            _Items.RemoveRange(_ItemIndex.Count, _Items.Count - _ItemIndex.Count);
        }

        public bool SetEquals(IEnumerable<T> other)
        {
            if (other == null)
            {
                return false;
            }
            var idx = 0;
            foreach (var oth in other)
            {
                if (idx >= Count)
                {
                    return false;
                }
                if (!_Comparer.Equals(oth, _Items[idx++]))
                {
                    return false;
                }
            }
            return idx == Count;
        }
    }
}