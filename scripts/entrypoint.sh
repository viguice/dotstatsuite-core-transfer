#!/bin/bash
# usage: file_env VAR [DEFAULT]
#    ie: file_env 'XYZ_DB_PASSWORD' 'example'
# (will allow for "$XYZ_DB_PASSWORD_FILE" to fill in the value of
#  "$XYZ_DB_PASSWORD" from a file, especially for Docker's secrets feature)
file_env() {
	local var="$1"
	local fileVar="${var}_FILE"
	local def="${2:-}"
	if [ "${!var:-}" ] && [ "${!fileVar:-}" ]; then
		#mysql_error "Both $var and $fileVar are set (but are exclusive)"
		echo "Both $var and $fileVar are set (but are exclusive)"
		exit -1
	fi
	local val="$def"
	if [ "${!var:-}" ]; then
		val="${!var}"
	elif [ "${!fileVar:-}" ]; then
		val="$(< "${!fileVar}")"
	fi
	export "$var"="$val"
	unset "$fileVar"
}


# Generic method looping in all variables.
# Meaning that _FILE suffix in a variable must be used only in this context.
for VAR_NAME_FILE in $(env | cut -f1 -d= | grep '_FILE$'); do
  if [[ -n "$VAR_NAME_FILE" ]]; then
    file_env "${VAR_NAME_FILE%_FILE}"
  fi
done
# Replace references to variables in ConnectionString 
for CONNECTION_NAME in $(env | cut -f1 -d= | grep 'ConnectionString$'); do
  echo "Replace variables in ${CONNECTION_NAME}"
  connection_name=${CONNECTION_NAME}
  if [[ -n "$CONNECTION_NAME" ]]; then
	for ENV_NAME in $(env | cut -f1 -d= | awk '{ print length($0) " " $0; }' | sort -n -r | cut -d ' ' -f 2-); do
    	export ${CONNECTION_NAME}=${!CONNECTION_NAME//\$${ENV_NAME}/${!ENV_NAME}}
	done
	echo "${CONNECTION_NAME}: ${!CONNECTION_NAME}"
  fi
done

dotnet DotStatServices.Transfer.dll 
