FROM mcr.microsoft.com/dotnet/sdk:6.0-alpine AS build
WORKDIR /app

# copy csproj and restore as distinct layers
COPY DotStat.Transfer/*.csproj ./DotStat.Transfer/
COPY DotStat.Transfer.Excel/*.csproj ./DotStat.Transfer.Excel/
COPY DotStatServices.Transfer/*.csproj ./DotStatServices.Transfer/
COPY DotStat.Transfer.Messaging.Consumer/*.csproj ./DotStat.Transfer.Messaging.Consumer/
COPY DotStat.Transfer.Messaging.Producer/*.csproj ./DotStat.Transfer.Messaging.Producer/
COPY nuget.config global.json version.json Directory.Build.props ./

# restore nuget packages
RUN dotnet restore DotStatServices.Transfer -nowarn:NU1605 -r linux-musl-x64

# copy everything else
COPY . /app

RUN dotnet publish DotStatServices.Transfer -nowarn:NU1605 -c Release --no-restore -r linux-musl-x64 -o /out

FROM mcr.microsoft.com/dotnet/aspnet:6.0-alpine AS runtime
WORKDIR /app
COPY --from=build /out .

#install extra libs needed in alpine based docker image, https://github.com/dotnet/dotnet-docker/issues/3844 
RUN apk update && apk add bash && apk add --no-cache icu-libs icu-data-full


ENV DOTNET_SYSTEM_GLOBALIZATION_INVARIANT=false

#Copy entry point shell script
COPY scripts/entrypoint.sh .
RUN chmod +x ./entrypoint.sh
#ENTRYPOINT ["dotnet", "DotStatServices.Transfer.dll"]
ENTRYPOINT ["./entrypoint.sh"]
